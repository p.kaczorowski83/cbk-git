// Webpack Imports
// import 'bootstrap'

// import { Fancybox } from "@fancyapps/ui";
// import pl from "@fancyapps/ui/src/Fancybox/l10n/pl";

// if ($('html').is(':lang(pl-pl)')) {
//     Fancybox.defaults.l10n = pl;
// }
/**
 * bxSlider v4.2.12
 * Copyright 2013-2015 Steven Wanderski
 * Written while drinking Belgian ales and listening to jazz
 * Licensed under MIT (http://opensource.org/licenses/MIT)
 */
! function(t) {
    var e = { mode: "horizontal", slideSelector: "", infiniteLoop: !0, hideControlOnEnd: !1, speed: 500, easing: null, slideMargin: 0, startSlide: 0, randomStart: !1, captions: !1, ticker: !1, tickerHover: !1, adaptiveHeight: !1, adaptiveHeightSpeed: 500, video: !1, useCSS: !0, preloadImages: "visible", responsive: !0, slideZIndex: 50, wrapperClass: "bx-wrapper", touchEnabled: !0, swipeThreshold: 50, oneToOneTouch: !0, preventDefaultSwipeX: !0, preventDefaultSwipeY: !1, ariaLive: !0, ariaHidden: !0, keyboardEnabled: !1, pager: !0, pagerType: "full", pagerShortSeparator: " / ", pagerSelector: null, buildPager: null, pagerCustom: null, controls: !0, nextText: "Next", prevText: "Prev", nextSelector: null, prevSelector: null, autoControls: !1, startText: "Start", stopText: "Stop", autoControlsCombine: !1, autoControlsSelector: null, auto: !1, pause: 4e3, autoStart: !0, autoDirection: "next", stopAutoOnClick: !1, autoHover: !1, autoDelay: 0, autoSlideForOnePage: !1, minSlides: 1, maxSlides: 1, moveSlides: 0, slideWidth: 0, shrinkItems: !1, onSliderLoad: function() { return !0 }, onSlideBefore: function() { return !0 }, onSlideAfter: function() { return !0 }, onSlideNext: function() { return !0 }, onSlidePrev: function() { return !0 }, onSliderResize: function() { return !0 } };
    t.fn.bxSlider = function(n) {
        if (0 === this.length) return this;
        if (this.length > 1) return this.each(function() { t(this).bxSlider(n) }), this;
        var s = {},
            o = this,
            r = t(window).width(),
            a = t(window).height();
        if (!t(o).data("bxSlider")) {
            var l = function() {
                    t(o).data("bxSlider") || (s.settings = t.extend({}, e, n), s.settings.slideWidth = parseInt(s.settings.slideWidth), s.children = o.children(s.settings.slideSelector), s.children.length < s.settings.minSlides && (s.settings.minSlides = s.children.length), s.children.length < s.settings.maxSlides && (s.settings.maxSlides = s.children.length), s.settings.randomStart && (s.settings.startSlide = Math.floor(Math.random() * s.children.length)), s.active = { index: s.settings.startSlide }, s.carousel = s.settings.minSlides > 1 || s.settings.maxSlides > 1, s.carousel && (s.settings.preloadImages = "all"), s.minThreshold = s.settings.minSlides * s.settings.slideWidth + (s.settings.minSlides - 1) * s.settings.slideMargin, s.maxThreshold = s.settings.maxSlides * s.settings.slideWidth + (s.settings.maxSlides - 1) * s.settings.slideMargin, s.working = !1, s.controls = {}, s.interval = null, s.animProp = "vertical" === s.settings.mode ? "top" : "left", s.usingCSS = s.settings.useCSS && "fade" !== s.settings.mode && function() {
                        for (var t = document.createElement("div"), e = ["WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"], i = 0; i < e.length; i++)
                            if (void 0 !== t.style[e[i]]) return s.cssPrefix = e[i].replace("Perspective", "").toLowerCase(), s.animProp = "-" + s.cssPrefix + "-transform", !0;
                        return !1
                    }(), "vertical" === s.settings.mode && (s.settings.maxSlides = s.settings.minSlides), o.data("origStyle", o.attr("style")), o.children(s.settings.slideSelector).each(function() { t(this).data("origStyle", t(this).attr("style")) }), d())
                },
                d = function() {
                    var e = s.children.eq(s.settings.startSlide);
                    o.wrap('<div class="' + s.settings.wrapperClass + '"><div class="bx-viewport"></div></div>'), s.viewport = o.parent(), s.settings.ariaLive && !s.settings.ticker && s.viewport.attr("aria-live", "polite"), s.loader = t('<div class="bx-loading" />'), s.viewport.prepend(s.loader), o.css({ width: "horizontal" === s.settings.mode ? 1e3 * s.children.length + 215 + "%" : "auto", position: "relative" }), s.usingCSS && s.settings.easing ? o.css("-" + s.cssPrefix + "-transition-timing-function", s.settings.easing) : s.settings.easing || (s.settings.easing = "swing"), s.viewport.css({ width: "100%", overflow: "hidden", position: "relative" }), s.viewport.parent().css({ maxWidth: u() }), s.children.css({ float: "horizontal" === s.settings.mode ? "left" : "none", listStyle: "none", position: "relative" }), s.children.css("width", h()), "horizontal" === s.settings.mode && s.settings.slideMargin > 0 && s.children.css("marginRight", s.settings.slideMargin), "vertical" === s.settings.mode && s.settings.slideMargin > 0 && s.children.css("marginBottom", s.settings.slideMargin), "fade" === s.settings.mode && (s.children.css({ position: "absolute", zIndex: 0, display: "none" }), s.children.eq(s.settings.startSlide).css({ zIndex: s.settings.slideZIndex, display: "flex" })), s.controls.el = t('<div class="bx-controls" />'), s.settings.captions && P(), s.active.last = s.settings.startSlide === f() - 1, s.settings.video && o.fitVids(), ("all" === s.settings.preloadImages || s.settings.ticker) && (e = s.children), s.settings.ticker ? s.settings.pager = !1 : (s.settings.controls && C(), s.settings.auto && s.settings.autoControls && T(), s.settings.pager && w(), (s.settings.controls || s.settings.autoControls || s.settings.pager) && s.viewport.after(s.controls.el)), c(e, g)
                },
                c = function(e, i) {
                    var n = e.find('img:not([src=""]), iframe').length,
                        s = 0;
                    return 0 === n ? void i() : void e.find('img:not([src=""]), iframe').each(function() { t(this).one("load error", function() {++s === n && i() }).each(function() { this.complete && t(this).trigger("load") }) })
                },
                g = function() {
                    if (s.settings.infiniteLoop && "fade" !== s.settings.mode && !s.settings.ticker) {
                        var e = "vertical" === s.settings.mode ? s.settings.minSlides : s.settings.maxSlides,
                            i = s.children.slice(0, e).clone(!0).addClass("bx-clone"),
                            n = s.children.slice(-e).clone(!0).addClass("bx-clone");
                        s.settings.ariaHidden && (i.attr("aria-hidden", !0), n.attr("aria-hidden", !0)), o.append(i).prepend(n)
                    }
                    s.loader.remove(), m(), "vertical" === s.settings.mode && (s.settings.adaptiveHeight = !0), s.viewport.height(p()), o.redrawSlider(), s.settings.onSliderLoad.call(o, s.active.index), s.initialized = !0, s.settings.responsive && t(window).bind("resize", Z), s.settings.auto && s.settings.autoStart && (f() > 1 || s.settings.autoSlideForOnePage) && H(), s.settings.ticker && W(), s.settings.pager && I(s.settings.startSlide), s.settings.controls && D(), s.settings.touchEnabled && !s.settings.ticker && N(), s.settings.keyboardEnabled && !s.settings.ticker && t(document).keydown(F)
                },
                p = function() {
                    var e = 0,
                        n = t();
                    if ("vertical" === s.settings.mode || s.settings.adaptiveHeight)
                        if (s.carousel) { var o = 1 === s.settings.moveSlides ? s.active.index : s.active.index * x(); for (n = s.children.eq(o), i = 1; i <= s.settings.maxSlides - 1; i++) n = o + i >= s.children.length ? n.add(s.children.eq(i - 1)) : n.add(s.children.eq(o + i)) } else n = s.children.eq(s.active.index);
                    else n = s.children;
                    return "vertical" === s.settings.mode ? (n.each(function(i) { e += t(this).outerHeight() }), s.settings.slideMargin > 0 && (e += s.settings.slideMargin * (s.settings.minSlides - 1))) : e = Math.max.apply(Math, n.map(function() { return t(this).outerHeight(!1) }).get()), "border-box" === s.viewport.css("box-sizing") ? e += parseFloat(s.viewport.css("padding-top")) + parseFloat(s.viewport.css("padding-bottom")) + parseFloat(s.viewport.css("border-top-width")) + parseFloat(s.viewport.css("border-bottom-width")) : "padding-box" === s.viewport.css("box-sizing") && (e += parseFloat(s.viewport.css("padding-top")) + parseFloat(s.viewport.css("padding-bottom"))), e
                },
                u = function() { var t = "100%"; return s.settings.slideWidth > 0 && (t = "horizontal" === s.settings.mode ? s.settings.maxSlides * s.settings.slideWidth + (s.settings.maxSlides - 1) * s.settings.slideMargin : s.settings.slideWidth), t },
                h = function() {
                    var t = s.settings.slideWidth,
                        e = s.viewport.width();
                    if (0 === s.settings.slideWidth || s.settings.slideWidth > e && !s.carousel || "vertical" === s.settings.mode) t = e;
                    else if (s.settings.maxSlides > 1 && "horizontal" === s.settings.mode) {
                        if (e > s.maxThreshold) return t;
                        e < s.minThreshold ? t = (e - s.settings.slideMargin * (s.settings.minSlides - 1)) / s.settings.minSlides : s.settings.shrinkItems && (t = Math.floor((e + s.settings.slideMargin) / Math.ceil((e + s.settings.slideMargin) / (t + s.settings.slideMargin)) - s.settings.slideMargin))
                    }
                    return t
                },
                v = function() {
                    var t = 1,
                        e = null;
                    return "horizontal" === s.settings.mode && s.settings.slideWidth > 0 ? s.viewport.width() < s.minThreshold ? t = s.settings.minSlides : s.viewport.width() > s.maxThreshold ? t = s.settings.maxSlides : (e = s.children.first().width() + s.settings.slideMargin, t = Math.floor((s.viewport.width() + s.settings.slideMargin) / e)) : "vertical" === s.settings.mode && (t = s.settings.minSlides), t
                },
                f = function() {
                    var t = 0,
                        e = 0,
                        i = 0;
                    if (s.settings.moveSlides > 0)
                        if (s.settings.infiniteLoop) t = Math.ceil(s.children.length / x());
                        else
                            for (; e < s.children.length;) ++t, e = i + v(), i += s.settings.moveSlides <= v() ? s.settings.moveSlides : v();
                    else t = Math.ceil(s.children.length / v());
                    return t
                },
                x = function() { return s.settings.moveSlides > 0 && s.settings.moveSlides <= v() ? s.settings.moveSlides : v() },
                m = function() {
                    var t, e, i;
                    s.children.length > s.settings.maxSlides && s.active.last && !s.settings.infiniteLoop ? "horizontal" === s.settings.mode ? (e = s.children.last(), t = e.position(), S(-(t.left - (s.viewport.width() - e.outerWidth())), "reset", 0)) : "vertical" === s.settings.mode && (i = s.children.length - s.settings.minSlides, t = s.children.eq(i).position(), S(-t.top, "reset", 0)) : (t = s.children.eq(s.active.index * x()).position(), s.active.index === f() - 1 && (s.active.last = !0), void 0 !== t && ("horizontal" === s.settings.mode ? S(-t.left, "reset", 0) : "vertical" === s.settings.mode && S(-t.top, "reset", 0)))
                },
                S = function(e, i, n, r) {
                    var a, l;
                    s.usingCSS ? (l = "vertical" === s.settings.mode ? "translate3d(0, " + e + "px, 0)" : "translate3d(" + e + "px, 0, 0)", o.css("-" + s.cssPrefix + "-transition-duration", n / 1e3 + "s"), "slide" === i ? (o.css(s.animProp, l), 0 !== n ? o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(e) { t(e.target).is(o) && (o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"), q()) }) : q()) : "reset" === i ? o.css(s.animProp, l) : "ticker" === i && (o.css("-" + s.cssPrefix + "-transition-timing-function", "linear"), o.css(s.animProp, l), 0 !== n ? o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(e) { t(e.target).is(o) && (o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"), S(r.resetValue, "reset", 0), L()) }) : (S(r.resetValue, "reset", 0), L()))) : (a = {}, a[s.animProp] = e, "slide" === i ? o.animate(a, n, s.settings.easing, function() { q() }) : "reset" === i ? o.css(s.animProp, e) : "ticker" === i && o.animate(a, n, "linear", function() { S(r.resetValue, "reset", 0), L() }))
                },
                b = function() {
                    for (var e = "", i = "", n = f(), o = 0; o < n; o++) i = "", s.settings.buildPager && t.isFunction(s.settings.buildPager) || s.settings.pagerCustom ? (i = s.settings.buildPager(o), s.pagerEl.addClass("bx-custom-pager")) : (i = o + 1, s.pagerEl.addClass("bx-default-pager")), e += '<div class="bx-pager-item"><a href="" data-slide-index="' + o + '" class="bx-pager-link">' + i + "</a></div>";
                    s.pagerEl.html(e)
                },
                w = function() { s.settings.pagerCustom ? s.pagerEl = t(s.settings.pagerCustom) : (s.pagerEl = t('<div class="bx-pager" />'), s.settings.pagerSelector ? t(s.settings.pagerSelector).html(s.pagerEl) : s.controls.el.addClass("bx-has-pager").append(s.pagerEl), b()), s.pagerEl.on("click touchend", "a", z) },
                C = function() { s.controls.next = t('<a class="bx-next" href="">' + s.settings.nextText + "</a>"), s.controls.prev = t('<a class="bx-prev" href="">' + s.settings.prevText + "</a>"), s.controls.next.bind("click touchend", E), s.controls.prev.bind("click touchend", k), s.settings.nextSelector && t(s.settings.nextSelector).append(s.controls.next), s.settings.prevSelector && t(s.settings.prevSelector).append(s.controls.prev), s.settings.nextSelector || s.settings.prevSelector || (s.controls.directionEl = t('<div class="bx-controls-direction" />'), s.controls.directionEl.append(s.controls.prev).append(s.controls.next), s.controls.el.addClass("bx-has-controls-direction").append(s.controls.directionEl)) },
                T = function() { s.controls.start = t('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + s.settings.startText + "</a></div>"), s.controls.stop = t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + s.settings.stopText + "</a></div>"), s.controls.autoEl = t('<div class="bx-controls-auto" />'), s.controls.autoEl.on("click", ".bx-start", M), s.controls.autoEl.on("click", ".bx-stop", y), s.settings.autoControlsCombine ? s.controls.autoEl.append(s.controls.start) : s.controls.autoEl.append(s.controls.start).append(s.controls.stop), s.settings.autoControlsSelector ? t(s.settings.autoControlsSelector).html(s.controls.autoEl) : s.controls.el.addClass("bx-has-controls-auto").append(s.controls.autoEl), A(s.settings.autoStart ? "stop" : "start") },
                P = function() {
                    s.children.each(function(e) {
                        var i = t(this).find("img:first").attr("title");
                        void 0 !== i && ("" + i).length && t(this).append('<div class="bx-caption"><span>' + i + "</span></div>")
                    })
                },
                E = function(t) { t.preventDefault(), s.controls.el.hasClass("disabled") || (s.settings.auto && s.settings.stopAutoOnClick && o.stopAuto(), o.goToNextSlide()) },
                k = function(t) { t.preventDefault(), s.controls.el.hasClass("disabled") || (s.settings.auto && s.settings.stopAutoOnClick && o.stopAuto(), o.goToPrevSlide()) },
                M = function(t) { o.startAuto(), t.preventDefault() },
                y = function(t) { o.stopAuto(), t.preventDefault() },
                z = function(e) {
                    var i, n;
                    e.preventDefault(), s.controls.el.hasClass("disabled") || (s.settings.auto && s.settings.stopAutoOnClick && o.stopAuto(), i = t(e.currentTarget), void 0 !== i.attr("data-slide-index") && (n = parseInt(i.attr("data-slide-index")), n !== s.active.index && o.goToSlide(n)))
                },
                I = function(e) { var i = s.children.length; return "short" === s.settings.pagerType ? (s.settings.maxSlides > 1 && (i = Math.ceil(s.children.length / s.settings.maxSlides)), void s.pagerEl.html(e + 1 + s.settings.pagerShortSeparator + i)) : (s.pagerEl.find("a").removeClass("active"), void s.pagerEl.each(function(i, n) { t(n).find("a").eq(e).addClass("active") })) },
                q = function() {
                    if (s.settings.infiniteLoop) {
                        var t = "";
                        0 === s.active.index ? t = s.children.eq(0).position() : s.active.index === f() - 1 && s.carousel ? t = s.children.eq((f() - 1) * x()).position() : s.active.index === s.children.length - 1 && (t = s.children.eq(s.children.length - 1).position()), t && ("horizontal" === s.settings.mode ? S(-t.left, "reset", 0) : "vertical" === s.settings.mode && S(-t.top, "reset", 0))
                    }
                    s.working = !1, s.settings.onSlideAfter.call(o, s.children.eq(s.active.index), s.oldIndex, s.active.index)
                },
                A = function(t) { s.settings.autoControlsCombine ? s.controls.autoEl.html(s.controls[t]) : (s.controls.autoEl.find("a").removeClass("active"), s.controls.autoEl.find("a:not(.bx-" + t + ")").addClass("active")) },
                D = function() { 1 === f() ? (s.controls.prev.addClass("disabled"), s.controls.next.addClass("disabled")) : !s.settings.infiniteLoop && s.settings.hideControlOnEnd && (0 === s.active.index ? (s.controls.prev.addClass("disabled"), s.controls.next.removeClass("disabled")) : s.active.index === f() - 1 ? (s.controls.next.addClass("disabled"), s.controls.prev.removeClass("disabled")) : (s.controls.prev.removeClass("disabled"), s.controls.next.removeClass("disabled"))) },
                H = function() {
                    if (s.settings.autoDelay > 0) { setTimeout(o.startAuto, s.settings.autoDelay) } else o.startAuto(), t(window).focus(function() { o.startAuto() }).blur(function() { o.stopAuto() });
                    s.settings.autoHover && o.hover(function() { s.interval && (o.stopAuto(!0), s.autoPaused = !0) }, function() { s.autoPaused && (o.startAuto(!0), s.autoPaused = null) })
                },
                W = function() { var e, i, n, r, a, l, d, c, g = 0; "next" === s.settings.autoDirection ? o.append(s.children.clone().addClass("bx-clone")) : (o.prepend(s.children.clone().addClass("bx-clone")), e = s.children.first().position(), g = "horizontal" === s.settings.mode ? -e.left : -e.top), S(g, "reset", 0), s.settings.pager = !1, s.settings.controls = !1, s.settings.autoControls = !1, s.settings.tickerHover && (s.usingCSS ? (r = "horizontal" === s.settings.mode ? 4 : 5, s.viewport.hover(function() { i = o.css("-" + s.cssPrefix + "-transform"), n = parseFloat(i.split(",")[r]), S(n, "reset", 0) }, function() { c = 0, s.children.each(function(e) { c += "horizontal" === s.settings.mode ? t(this).outerWidth(!0) : t(this).outerHeight(!0) }), a = s.settings.speed / c, l = "horizontal" === s.settings.mode ? "left" : "top", d = a * (c - Math.abs(parseInt(n))), L(d) })) : s.viewport.hover(function() { o.stop() }, function() { c = 0, s.children.each(function(e) { c += "horizontal" === s.settings.mode ? t(this).outerWidth(!0) : t(this).outerHeight(!0) }), a = s.settings.speed / c, l = "horizontal" === s.settings.mode ? "left" : "top", d = a * (c - Math.abs(parseInt(o.css(l)))), L(d) })), L() },
                L = function(t) {
                    var e, i, n, r = t ? t : s.settings.speed,
                        a = { left: 0, top: 0 },
                        l = { left: 0, top: 0 };
                    "next" === s.settings.autoDirection ? a = o.find(".bx-clone").first().position() : l = s.children.first().position(), e = "horizontal" === s.settings.mode ? -a.left : -a.top, i = "horizontal" === s.settings.mode ? -l.left : -l.top, n = { resetValue: i }, S(e, "ticker", r, n)
                },
                O = function(e) {
                    var i = t(window),
                        n = { top: i.scrollTop(), left: i.scrollLeft() },
                        s = e.offset();
                    return n.right = n.left + i.width(), n.bottom = n.top + i.height(), s.right = s.left + e.outerWidth(), s.bottom = s.top + e.outerHeight(), !(n.right < s.left || n.left > s.right || n.bottom < s.top || n.top > s.bottom)
                },
                F = function(t) {
                    var e = document.activeElement.tagName.toLowerCase(),
                        i = "input|textarea",
                        n = new RegExp(e, ["i"]),
                        s = n.exec(i);
                    if (null == s && O(o)) { if (39 === t.keyCode) return E(t), !1; if (37 === t.keyCode) return k(t), !1 }
                },
                N = function() { s.touch = { start: { x: 0, y: 0 }, end: { x: 0, y: 0 } }, s.viewport.bind("touchstart MSPointerDown pointerdown", X), s.viewport.on("click", ".bxslider a", function(t) { s.viewport.hasClass("click-disabled") && (t.preventDefault(), s.viewport.removeClass("click-disabled")) }) },
                X = function(t) {
                    if (s.controls.el.addClass("disabled"), s.working) t.preventDefault(), s.controls.el.removeClass("disabled");
                    else {
                        s.touch.originalPos = o.position();
                        var e = t.originalEvent,
                            i = "undefined" != typeof e.changedTouches ? e.changedTouches : [e];
                        s.touch.start.x = i[0].pageX, s.touch.start.y = i[0].pageY, s.viewport.get(0).setPointerCapture && (s.pointerId = e.pointerId, s.viewport.get(0).setPointerCapture(s.pointerId)), s.viewport.bind("touchmove MSPointerMove pointermove", V), s.viewport.bind("touchend MSPointerUp pointerup", R), s.viewport.bind("MSPointerCancel pointercancel", Y)
                    }
                },
                Y = function(t) { S(s.touch.originalPos.left, "reset", 0), s.controls.el.removeClass("disabled"), s.viewport.unbind("MSPointerCancel pointercancel", Y), s.viewport.unbind("touchmove MSPointerMove pointermove", V), s.viewport.unbind("touchend MSPointerUp pointerup", R), s.viewport.get(0).releasePointerCapture && s.viewport.get(0).releasePointerCapture(s.pointerId) },
                V = function(t) {
                    var e = t.originalEvent,
                        i = "undefined" != typeof e.changedTouches ? e.changedTouches : [e],
                        n = Math.abs(i[0].pageX - s.touch.start.x),
                        o = Math.abs(i[0].pageY - s.touch.start.y),
                        r = 0,
                        a = 0;
                    3 * n > o && s.settings.preventDefaultSwipeX ? t.preventDefault() : 3 * o > n && s.settings.preventDefaultSwipeY && t.preventDefault(), "fade" !== s.settings.mode && s.settings.oneToOneTouch && ("horizontal" === s.settings.mode ? (a = i[0].pageX - s.touch.start.x, r = s.touch.originalPos.left + a) : (a = i[0].pageY - s.touch.start.y, r = s.touch.originalPos.top + a), S(r, "reset", 0))
                },
                R = function(t) {
                    s.viewport.unbind("touchmove MSPointerMove pointermove", V), s.controls.el.removeClass("disabled");
                    var e = t.originalEvent,
                        i = "undefined" != typeof e.changedTouches ? e.changedTouches : [e],
                        n = 0,
                        r = 0;
                    s.touch.end.x = i[0].pageX, s.touch.end.y = i[0].pageY, "fade" === s.settings.mode ? (r = Math.abs(s.touch.start.x - s.touch.end.x), r >= s.settings.swipeThreshold && (s.touch.start.x > s.touch.end.x ? o.goToNextSlide() : o.goToPrevSlide(), o.stopAuto())) : ("horizontal" === s.settings.mode ? (r = s.touch.end.x - s.touch.start.x, n = s.touch.originalPos.left) : (r = s.touch.end.y - s.touch.start.y, n = s.touch.originalPos.top), !s.settings.infiniteLoop && (0 === s.active.index && r > 0 || s.active.last && r < 0) ? S(n, "reset", 200) : Math.abs(r) >= s.settings.swipeThreshold ? (r < 0 ? o.goToNextSlide() : o.goToPrevSlide(), o.stopAuto()) : S(n, "reset", 200)), s.viewport.unbind("touchend MSPointerUp pointerup", R), s.viewport.get(0).releasePointerCapture && s.viewport.get(0).releasePointerCapture(s.pointerId)
                },
                Z = function(e) {
                    if (s.initialized)
                        if (s.working) window.setTimeout(Z, 10);
                        else {
                            var i = t(window).width(),
                                n = t(window).height();
                            r === i && a === n || (r = i, a = n, o.redrawSlider(), s.settings.onSliderResize.call(o, s.active.index))
                        }
                },
                B = function(t) {
                    var e = v();
                    s.settings.ariaHidden && !s.settings.ticker && (s.children.attr("aria-hidden", "true"), s.children.slice(t, t + e).attr("aria-hidden", "false"))
                },
                U = function(t) { return t < 0 ? s.settings.infiniteLoop ? f() - 1 : s.active.index : t >= f() ? s.settings.infiniteLoop ? 0 : s.active.index : t };
            return o.goToSlide = function(e, i) {
                var n, r, a, l, d = !0,
                    c = 0,
                    g = { left: 0, top: 0 },
                    u = null;
                if (s.oldIndex = s.active.index, s.active.index = U(e), !s.working && s.active.index !== s.oldIndex) { if (s.working = !0, d = s.settings.onSlideBefore.call(o, s.children.eq(s.active.index), s.oldIndex, s.active.index), "undefined" != typeof d && !d) return s.active.index = s.oldIndex, void(s.working = !1); "next" === i ? s.settings.onSlideNext.call(o, s.children.eq(s.active.index), s.oldIndex, s.active.index) || (d = !1) : "prev" === i && (s.settings.onSlidePrev.call(o, s.children.eq(s.active.index), s.oldIndex, s.active.index) || (d = !1)), s.active.last = s.active.index >= f() - 1, (s.settings.pager || s.settings.pagerCustom) && I(s.active.index), s.settings.controls && D(), "fade" === s.settings.mode ? (s.settings.adaptiveHeight && s.viewport.height() !== p() && s.viewport.animate({ height: p() }, s.settings.adaptiveHeightSpeed), s.children.filter(":visible").fadeOut(s.settings.speed).css({ zIndex: 0 }), s.children.eq(s.active.index).css("zIndex", s.settings.slideZIndex + 1).fadeIn(s.settings.speed, function() { t(this).css("zIndex", s.settings.slideZIndex), q() })) : (s.settings.adaptiveHeight && s.viewport.height() !== p() && s.viewport.animate({ height: p() }, s.settings.adaptiveHeightSpeed), !s.settings.infiniteLoop && s.carousel && s.active.last ? "horizontal" === s.settings.mode ? (u = s.children.eq(s.children.length - 1), g = u.position(), c = s.viewport.width() - u.outerWidth()) : (n = s.children.length - s.settings.minSlides, g = s.children.eq(n).position()) : s.carousel && s.active.last && "prev" === i ? (r = 1 === s.settings.moveSlides ? s.settings.maxSlides - x() : (f() - 1) * x() - (s.children.length - s.settings.maxSlides), u = o.children(".bx-clone").eq(r), g = u.position()) : "next" === i && 0 === s.active.index ? (g = o.find("> .bx-clone").eq(s.settings.maxSlides).position(), s.active.last = !1) : e >= 0 && (l = e * parseInt(x()), g = s.children.eq(l).position()), "undefined" != typeof g ? (a = "horizontal" === s.settings.mode ? -(g.left - c) : -g.top, S(a, "slide", s.settings.speed)) : s.working = !1), s.settings.ariaHidden && B(s.active.index * x()) }
            }, o.goToNextSlide = function() {
                if (s.settings.infiniteLoop || !s.active.last) {
                    var t = parseInt(s.active.index) + 1;
                    o.goToSlide(t, "next")
                }
            }, o.goToPrevSlide = function() {
                if (s.settings.infiniteLoop || 0 !== s.active.index) {
                    var t = parseInt(s.active.index) - 1;
                    o.goToSlide(t, "prev")
                }
            }, o.startAuto = function(t) { s.interval || (s.interval = setInterval(function() { "next" === s.settings.autoDirection ? o.goToNextSlide() : o.goToPrevSlide() }, s.settings.pause), s.settings.autoControls && t !== !0 && A("stop")) }, o.stopAuto = function(t) { s.interval && (clearInterval(s.interval), s.interval = null, s.settings.autoControls && t !== !0 && A("start")) }, o.getCurrentSlide = function() { return s.active.index }, o.getCurrentSlideElement = function() { return s.children.eq(s.active.index) }, o.getSlideElement = function(t) { return s.children.eq(t) }, o.getSlideCount = function() { return s.children.length }, o.isWorking = function() { return s.working }, o.redrawSlider = function() { s.children.add(o.find(".bx-clone")).outerWidth(h()), s.viewport.css("height", p()), s.settings.ticker || m(), s.active.last && (s.active.index = f() - 1), s.active.index >= f() && (s.active.last = !0), s.settings.pager && !s.settings.pagerCustom && (b(), I(s.active.index)), s.settings.ariaHidden && B(s.active.index * x()) }, o.destroySlider = function() { s.initialized && (s.initialized = !1, t(".bx-clone", this).remove(), s.children.each(function() { void 0 !== t(this).data("origStyle") ? t(this).attr("style", t(this).data("origStyle")) : t(this).removeAttr("style") }), void 0 !== t(this).data("origStyle") ? this.attr("style", t(this).data("origStyle")) : t(this).removeAttr("style"), t(this).unwrap().unwrap(), s.controls.el && s.controls.el.remove(), s.controls.next && s.controls.next.remove(), s.controls.prev && s.controls.prev.remove(), s.pagerEl && s.settings.controls && !s.settings.pagerCustom && s.pagerEl.remove(), t(".bx-caption", this).remove(), s.controls.autoEl && s.controls.autoEl.remove(), clearInterval(s.interval), s.settings.responsive && t(window).unbind("resize", Z), s.settings.keyboardEnabled && t(document).unbind("keydown", F), t(this).removeData("bxSlider")) }, o.reloadSlider = function(e) { void 0 !== e && (n = e), o.destroySlider(), l(), t(o).data("bxSlider", this) }, l(), t(o).data("bxSlider", this), this
        }
    }
}(jQuery);


(function(factory) { // eslint-disable-line no-extra-semi
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Global
        factory(jQuery);
    }
})(function($) {
    /*
    *  internal
    */

    var _previousResizeWidth = -1,
        _updateTimeout = -1;

    /*
    *  _parse
    *  value parse utility function
    */

    var _parse = function(value) {
        // parse value and convert NaN to 0
        return parseFloat(value) || 0;
    };

    /*
    *  _rows
    *  utility function returns array of jQuery selections representing each row
    *  (as displayed after float wrapping applied by browser)
    */

    var _rows = function(elements) {
        var tolerance = 1,
            $elements = $(elements),
            lastTop = null,
            rows = [];

        // group elements by their top position
        $elements.each(function(){
            var $that = $(this),
                top = $that.offset().top - _parse($that.css('margin-top')),
                lastRow = rows.length > 0 ? rows[rows.length - 1] : null;

            if (lastRow === null) {
                // first item on the row, so just push it
                rows.push($that);
            } else {
                // if the row top is the same, add to the row group
                if (Math.floor(Math.abs(lastTop - top)) <= tolerance) {
                    rows[rows.length - 1] = lastRow.add($that);
                } else {
                    // otherwise start a new row group
                    rows.push($that);
                }
            }

            // keep track of the last row top
            lastTop = top;
        });

        return rows;
    };

    /*
    *  _parseOptions
    *  handle plugin options
    */

    var _parseOptions = function(options) {
        var opts = {
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        };

        if (typeof options === 'object') {
            return $.extend(opts, options);
        }

        if (typeof options === 'boolean') {
            opts.byRow = options;
        } else if (options === 'remove') {
            opts.remove = true;
        }

        return opts;
    };

    /*
    *  matchHeight
    *  plugin definition
    */

    var matchHeight = $.fn.matchHeight = function(options) {
        var opts = _parseOptions(options);

        // handle remove
        if (opts.remove) {
            var that = this;

            // remove fixed height from all selected elements
            this.css(opts.property, '');

            // remove selected elements from all groups
            $.each(matchHeight._groups, function(key, group) {
                group.elements = group.elements.not(that);
            });

            // TODO: cleanup empty groups

            return this;
        }

        if (this.length <= 1 && !opts.target) {
            return this;
        }

        // keep track of this group so we can re-apply later on load and resize events
        matchHeight._groups.push({
            elements: this,
            options: opts
        });

        // match each element's height to the tallest element in the selection
        matchHeight._apply(this, opts);

        return this;
    };

    /*
    *  plugin global options
    */

    matchHeight.version = 'master';
    matchHeight._groups = [];
    matchHeight._throttle = 80;
    matchHeight._maintainScroll = false;
    matchHeight._beforeUpdate = null;
    matchHeight._afterUpdate = null;
    matchHeight._rows = _rows;
    matchHeight._parse = _parse;
    matchHeight._parseOptions = _parseOptions;

    /*
    *  matchHeight._apply
    *  apply matchHeight to given elements
    */

    matchHeight._apply = function(elements, options) {
        var opts = _parseOptions(options),
            $elements = $(elements),
            rows = [$elements];

        // take note of scroll position
        var scrollTop = $(window).scrollTop(),
            htmlHeight = $('html').outerHeight(true);

        // get hidden parents
        var $hiddenParents = $elements.parents().filter(':hidden');

        // cache the original inline style
        $hiddenParents.each(function() {
            var $that = $(this);
            $that.data('style-cache', $that.attr('style'));
        });

        // temporarily must force hidden parents visible
        $hiddenParents.css('display', 'block');

        // get rows if using byRow, otherwise assume one row
        if (opts.byRow && !opts.target) {

            // must first force an arbitrary equal height so floating elements break evenly
            $elements.each(function() {
                var $that = $(this),
                    display = $that.css('display');

                // temporarily force a usable display value
                if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                    display = 'block';
                }

                // cache the original inline style
                $that.data('style-cache', $that.attr('style'));

                $that.css({
                    'display': display,
                    'padding-top': '0',
                    'padding-bottom': '0',
                    'margin-top': '0',
                    'margin-bottom': '0',
                    'border-top-width': '0',
                    'border-bottom-width': '0',
                    'height': '100px',
                    'overflow': 'hidden'
                });
            });

            // get the array of rows (based on element top position)
            rows = _rows($elements);

            // revert original inline styles
            $elements.each(function() {
                var $that = $(this);
                $that.attr('style', $that.data('style-cache') || '');
            });
        }

        $.each(rows, function(key, row) {
            var $row = $(row),
                targetHeight = 0;

            if (!opts.target) {
                // skip apply to rows with only one item
                if (opts.byRow && $row.length <= 1) {
                    $row.css(opts.property, '');
                    return;
                }

                // iterate the row and find the max height
                $row.each(function(){
                    var $that = $(this),
                        style = $that.attr('style'),
                        display = $that.css('display');

                    // temporarily force a usable display value
                    if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                        display = 'block';
                    }

                    // ensure we get the correct actual height (and not a previously set height value)
                    var css = { 'display': display };
                    css[opts.property] = '';
                    $that.css(css);

                    // find the max height (including padding, but not margin)
                    if ($that.outerHeight(false) > targetHeight) {
                        targetHeight = $that.outerHeight(false);
                    }

                    // revert styles
                    if (style) {
                        $that.attr('style', style);
                    } else {
                        $that.css('display', '');
                    }
                });
            } else {
                // if target set, use the height of the target element
                targetHeight = opts.target.outerHeight(false);
            }

            // iterate the row and apply the height to all elements
            $row.each(function(){
                var $that = $(this),
                    verticalPadding = 0;

                // don't apply to a target
                if (opts.target && $that.is(opts.target)) {
                    return;
                }

                // handle padding and border correctly (required when not using border-box)
                if ($that.css('box-sizing') !== 'border-box') {
                    verticalPadding += _parse($that.css('border-top-width')) + _parse($that.css('border-bottom-width'));
                    verticalPadding += _parse($that.css('padding-top')) + _parse($that.css('padding-bottom'));
                }

                // set the height (accounting for padding and border)
                $that.css(opts.property, (targetHeight - verticalPadding) + 'px');
            });
        });

        // revert hidden parents
        $hiddenParents.each(function() {
            var $that = $(this);
            $that.attr('style', $that.data('style-cache') || null);
        });

        // restore scroll position if enabled
        if (matchHeight._maintainScroll) {
            $(window).scrollTop((scrollTop / htmlHeight) * $('html').outerHeight(true));
        }

        return this;
    };

    /*
    *  matchHeight._applyDataApi
    *  applies matchHeight to all elements with a data-match-height attribute
    */

    matchHeight._applyDataApi = function() {
        var groups = {};

        // generate groups by their groupId set by elements using data-match-height
        $('[data-match-height], [data-mh]').each(function() {
            var $this = $(this),
                groupId = $this.attr('data-mh') || $this.attr('data-match-height');

            if (groupId in groups) {
                groups[groupId] = groups[groupId].add($this);
            } else {
                groups[groupId] = $this;
            }
        });

        // apply matchHeight to each group
        $.each(groups, function() {
            this.matchHeight(true);
        });
    };

    /*
    *  matchHeight._update
    *  updates matchHeight on all current groups with their correct options
    */

    var _update = function(event) {
        if (matchHeight._beforeUpdate) {
            matchHeight._beforeUpdate(event, matchHeight._groups);
        }

        $.each(matchHeight._groups, function() {
            matchHeight._apply(this.elements, this.options);
        });

        if (matchHeight._afterUpdate) {
            matchHeight._afterUpdate(event, matchHeight._groups);
        }
    };

    matchHeight._update = function(throttle, event) {
        // prevent update if fired from a resize event
        // where the viewport width hasn't actually changed
        // fixes an event looping bug in IE8
        if (event && event.type === 'resize') {
            var windowWidth = $(window).width();
            if (windowWidth === _previousResizeWidth) {
                return;
            }
            _previousResizeWidth = windowWidth;
        }

        // throttle updates
        if (!throttle) {
            _update(event);
        } else if (_updateTimeout === -1) {
            _updateTimeout = setTimeout(function() {
                _update(event);
                _updateTimeout = -1;
            }, matchHeight._throttle);
        }
    };

    /*
    *  bind events
    */

    // apply on DOM ready event
    $(matchHeight._applyDataApi);

    // use on or bind where supported
    var on = $.fn.on ? 'on' : 'bind';

    // update heights on load and resize events
    $(window)[on]('load', function(event) {
        matchHeight._update(false, event);
    });

    // throttled update heights on resize events
    $(window)[on]('resize orientationchange', function(event) {
        matchHeight._update(true, event);
    });

});


(function($) {


     // matchHeight
    $('.gallery__coll p').matchHeight({
       property: 'min-height'
    });

    $('.slider ul').bxSlider({
    mode: 'fade',
    captions: true,
     touchEnabled: false
  });



    if ('loading' in HTMLImageElement.prototype) {
        const images = document.querySelectorAll('img[loading="lazy"]');
        images.forEach(img => {
            img.src = img.dataset.src;
        });
    } else {
        // Dynamically import the LazySizes library
        const script = document.createElement('script');
        script.src =
            'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.2/lazysizes.min.js';
        document.body.appendChild(script);
    }


    // PLACEHOLDER
    $('input,textarea').focus(function() {
        $(this).data('placeholder', $(this).attr('placeholder'))
            .attr('placeholder', '');
    }).blur(function() {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });

    // SEARCH WIDGET 
    $('.navbar__fixed-search').click(function() {
        $('.search__widget').addClass('search__widget-show');
    });

    $('.search__widget-close').click(function() {
        $('.search__widget').removeClass('search__widget-show');
    });

    $('#dziedzictwo, #finanse, #prawo, #zarzadzanie').click(function() {
        // $("#txtAge").toggle(this.checked);

        // Using a pure CSS selector
        if ($(this.checked)) {
            // $('#post-baza-wiedzy').remove();
            // $('.search__widget-input').append('<input type="hidden" name="post_type[]" value="baza_wiedzy" />');
        }
    });

    $('#publikacje, #aktyprawne, #rewitalizacja').click(function() {
        $('#post-baza-wiedzy').remove();
    });

    // MENU SIZE - SCROLL DOWN
    var menu = $('body').offset().top + 200

    $(window).scroll(function() {
        if ($(this).scrollTop() >= menu) {
            $('.navbar__fixed').addClass('navbar__fixed--small');
            $('.mobile-overlay').addClass('mobile-overlay--scroll');
        } else {
            $('.navbar__fixed').removeClass('navbar__fixed--small');
            $('.mobile-overlay').removeClass('mobile-overlay--scroll');
        }
    });

    // HAMBURGER
    $('.hamburger').on('click', function() {
        $(this).toggleClass('open-h');
        $('body, html').toggleClass('menu-open');
        $('.mobile-overlay').stop().slideToggle().delay(300).toggleClass('open-m');
        $('.mobile-overlay .nav_mobile li:nth-child(1)').delay(400).animate({ opacity: 1 }, 100);
        $('.mobile-overlay .nav_mobile li:nth-child(2)').delay(600).animate({ opacity: 1 }, 100);
        $('.mobile-overlay .nav_mobile li:nth-child(3)').delay(800).animate({ opacity: 1 }, 100);
        $('.mobile-overlay .nav_mobile li:nth-child(4)').delay(1000).animate({ opacity: 1 }, 100);
        $('.mobile-overlay .nav_mobile li:nth-child(5)').delay(1200).animate({ opacity: 1 }, 100);
        $('.mobile-overlay .nav_mobile li:nth-child(6)').delay(1400).animate({ opacity: 1 }, 100);
        $('.mobile-overlay-lang').delay(1600).animate({ opacity: 1 }, 100);
        $('.open-m p').delay(1600).animate({ opacity: 1 }, 100);
        $('.open-m .search__widget-cnt').delay(1600).animate({ opacity: 1 }, 100);
        $('.open-m .search__widget-go').delay(1800).animate({ opacity: 1 }, 100);
    });

    $(window).on("load", function() {
        $('.load').css('opacity', '1');
    });

    // TAB
    document.body.addEventListener('mousedown', function() {
        document.body.classList.add('using-mouse');
    });

    document.body.addEventListener('keydown', function() {
        document.body.classList.remove('using-mouse');
    });

    // // FAQ
    // var action = "click";
    // var speed = "500";

    // $('li.q').on(action, function() {
    //     // Get next element
    //     $('li.q').removeClass('s')
    //     $(this).addClass('s')
    //     $(this).next()
    //         .slideToggle(speed)
    //         .addClass('b')
    //         .siblings('li.a')
    //         .slideUp();
    // });

    var allFaq = $('.patient__faq li div').hide();

    $('.patient__faq .q').click(function() {
      $(this).toggleClass('s');

       var panel = $(this).next();

      if (panel.css("display") == "block") {
          panel.slideUp();
      } else {
          $('.patient__faq .q').removeClass('s');
          $(this).addClass('s');
          allFaq.slideUp();
          panel.slideDown();
      }
  });

     // PARTER
     $('.page-id-393 #ph01a, .page-id-393 #ph01, .page-id-781 #ph01a, .page-id-781 #ph01').click(function(e) {
        e.preventDefault();
        $('#btn-1:eq(0)').click();
    });

    $('.page-id-393 #ph07a, .page-id-393 #ph07, .page-id-781 #ph07a, .page-id-781 #ph07').click(function(e) {
        e.preventDefault();
        $('#btn-2:eq(0)').click();
    });

    $('.page-id-393 #ph06a, .page-id-393 #ph06, .page-id-781 #ph06a, .page-id-781 #ph06').click(function(e) {
        e.preventDefault();
        $('#btn-3:eq(0)').click();
    });

    $('.page-id-393 #ph05a, .page-id-393 #ph05, .page-id-781 #ph05a, .page-id-781 #ph05').click(function(e) {
        e.preventDefault();
        $('#btn-4:eq(0)').click();
    });

     $('.page-id-393 #ph04a, .page-id-393 #ph04, .page-id-781 #ph04a, .page-id-781 #ph04').click(function(e) {
        e.preventDefault();
        $('#btn-5:eq(0)').click();
    });

     $('.page-id-393 #ph02a, .page-id-393 #ph02, .page-id-781 #ph02a, .page-id-781 #ph02').click(function(e) {
        e.preventDefault();0
        $('#btn-6:eq(0)').click();
    });

    //  $('.page-id-393 #ph02a, .page-id-393 #ph02, .page-id-781 #ph02a, .page-id-781 #ph02').click(function(e) {
    //     e.preventDefault();
    //     $('#btn-7:eq(0)').click();
    // });

     //  $('.page-id-393 #ph07a, .page-id-393 #ph07').on('click', function() {
     //    $('#gallery-2 a').click();
     // });

     //  $('.page-id-393 #ph06a, .page-id-393 #ph06').on('click', function() {
     //    $('#gallery-3 a').click();
     // });

     //   $('.page-id-393 #ph05a, .page-id-393 #ph05').on('click', function() {
     //    $('#gallery-4 a').click();
     // });

     //    $('.page-id-393 #ph04a, .page-id-393 #ph04').on('click', function() {
     //    $('#gallery-5 a').click();
     // });

     //     $('.page-id-393 #ph03a, .page-id-393 #ph03').on('click', function() {
     //    $('#gallery-6 a').click();
     // });

     //      $('.page-id-393 #ph02a, .page-id-393 #ph02').on('click', function() {
     //    $('#gallery-7 a').click();
     // });


    // PIĘTRO 1

    // $('.page-id-396 #ph01a, .page-id-396 #ph01').on('click', function() {
    //     $('#gallery-1 a').click();
    // });

    $('.page-id-396 #ph01a, .page-id-396 #ph01, .page-id-794 #ph01a, .page-id-794 #ph01').click(function(e) {
        e.preventDefault();
        $('#btn-1:eq(0)').click();
    });

     $('.page-id-396 #ph02a, .page-id-396 #ph02, .page-id-794 #ph02a, .page-id-794 #ph02').click(function(e) {
        e.preventDefault();
        $('#btn-2:eq(0)').click();
    });

       $('.page-id-396 #ph07a, .page-id-396 #ph07, .page-id-794 #ph07a, .page-id-794 #ph07').click(function(e) {
        e.preventDefault();
        $('#btn-3:eq(0)').click();
        return false;
    });

        $('.page-id-396 #ph06a, .page-id-396 #ph06, .page-id-794 #ph06a, .page-id-794 #ph06').click(function(e) {
        e.preventDefault();
        $('#btn-4:eq(0)').click();
        return false;
    });

    $('.page-id-396 #ph03a, .page-id-396 #ph03, .page-id-794 #ph03a, .page-id-794 #ph03').click(function(e) {
        e.preventDefault();
        $('#btn-5:eq(0)').click();
        return false;
    });

    $('.page-id-396 #ph05a, .page-id-396 #ph05, .page-id-794 #ph05a, .page-id-794 #ph05').click(function(e) {
        e.preventDefault();
        $('#btn-6:eq(0)').click();
        return false;
    });

    $('.page-id-396 #ph04a, .page-id-396 #ph04, .page-id-794 #ph04a, .page-id-794 #ph04').click(function(e) {
        e.preventDefault();
        $('#btn-7:eq(0)').click();
        return false;
    });

    // $('.page-id-396 #ph02a, .page-id-396 #ph02').on('click', function() {
    //    $('#gallery-2 a').click();
    // });

    // $('.page-id-396 #ph07a, .page-id-396 #ph07').on('click', function() {
    //    $('#gallery-3 a').click();
    // });

    // $('.page-id-396 #ph06a, .page-id-396 #ph06').on('click', function() {
    //    $('#gallery-4 a').click();
    // });

    // $('.page-id-396 #ph03a, .page-id-396 #ph03').on('click', function() {
    //    $('#gallery-5 a').click();
    // });

    // $('.page-id-396 #ph05a, .page-id-396 #ph05').on('click', function() {
    //    $('#gallery-6 a').click();
    // });

    // $('.page-id-396 #ph04a, .page-id-396 #ph04').on('click', function() {
    //    $('#gallery-7 a').click();
    // });

     $('.page-id-398 #ph01a, .page-id-398 #ph01, .page-id-806 #ph01a, .page-id-806 #ph01').click(function(e) {
        e.preventDefault();
        $('#btn-1:eq(0)').click();
        return false;
    });

     $('.page-id-398 #ph02a, .page-id-398 #ph02, .page-id-806 #ph02a, .page-id-806 #ph02').click(function(e) {
        e.preventDefault();
        $('#btn-2:eq(0)').click();
        return false;
    });

      $('.page-id-398 #ph03a, .page-id-398 #ph03, .page-id-806 #ph03a, .page-id-806 #ph03').click(function(e) {
        e.preventDefault();
        $('#btn-3:eq(0)').click();
        return false;
    });

      $('.page-id-398 #ph04a, .page-id-398 #ph04, .page-id-806 #ph04a, .page-id-806 #ph04').click(function(e) {
        e.preventDefault();
        $('#btn-4:eq(0)').click();
        return false;
    });

       $('.page-id-398 #ph05a, .page-id-398 #ph05, .page-id-806 #ph05a, .page-id-806 #ph05').click(function(e) {
        e.preventDefault();
        $('#btn-5:eq(0)').click();
        return false;
    });

          $('.page-id-398 #ph06a, .page-id-398 #ph06, .page-id-806 #ph06a, .page-id-806 #ph06').click(function(e) {
        e.preventDefault();
        $('#btn-6:eq(0)').click();
        return false;
    });
    

    $('.page-id-398 #ph07a, .page-id-398 #ph07, .page-id-806 #ph07a, .page-id-806 #ph07').click(function(e) {
        e.preventDefault();
        $('#btn-7:eq(0)').click();
        return false;
    });


    // $('.page-id-398 #ph01a, .page-id-398 #ph01').on('click', function() {
    //    $('#gallery-1 a').click();
    // });

    // $('.page-id-398 #ph02a, .page-id-398 #ph02').on('click', function() {
    //    $('#gallery-2 a').click();
    // });

    //  $('.page-id-398 #ph03a, .page-id-398 #ph03').on('click', function() {
    //    $('#gallery-3 a').click();
    // });

    //  $('.page-id-398 #ph04a, .page-id-398 #ph04').on('click', function() {
    //    $('#gallery-4 a').click();
    // });

    // $('.page-id-398 #ph05a, .page-id-398 #ph05').on('click', function() {
    //    $('#gallery-5 a').click();
    // });

    // $('.page-id-398 #ph06a, .page-id-398 #ph06').on('click', function() {
    //    $('#gallery-6 a').click();
    // });

    //  $('.page-id-398 #ph07a, .page-id-398 #ph07').on('click', function() {
    //    $('#gallery-7 a').click();
    // });


     $( ".dropdown" ).hover(
    function() {
        $(this).find('ul').show();
        }, function() {
        $(this).find('ul').hide();
        }
      );

var allListElements = $( "ul" );

$('.mobile-overlay .nav_mobile li ul').hide();

     $('#menu-item-541').on('click',function(){
  if(!$(this).data('clicked'))
    {
      $('.sub-menu').slideUp();
      $(this).find(allListElements).slideDown();
      $(this).data('clicked', true);
    }
  else
    {
     $(this).find(allListElements).slideUp();
      $(this).data('clicked', false);
    }
});

     $('#menu-item-543').on('click',function(){
  if(!$(this).data('clicked'))
    {
      $('.sub-menu').slideUp();
      $(this).find(allListElements).slideDown();
      $(this).data('clicked', true);
    }
  else
    {
     $(this).find(allListElements).slideUp();
      $(this).data('clicked', false);
    }
});

     $('#menu-item-542').on('click',function(){
  if(!$(this).data('clicked'))
    {
      $('.sub-menu').slideUp();
      $(this).find(allListElements).slideDown();
      $(this).data('clicked', true);
    }
  else
    {
     $(this).find(allListElements).slideUp();
      $(this).data('clicked', false);
    }
});

     $('#menu-item-544').on('click',function(){
  if(!$(this).data('clicked'))
    {
      $('.sub-menu').slideUp();
      $(this).find(allListElements).slideDown();
      $(this).data('clicked', true);
    }
  else
    {
     $(this).find(allListElements).slideUp();
      $(this).data('clicked', false);
    }
});


$('.mobile-overlay .menu li ul li a').on("click", function(){
    $('.mobile-overlay').stop().slideUp();
    $('.hamburger').toggleClass('open-h');
});

$( "#ph01" ).hover(
  function() {
    $( '#ph01a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph01a').removeClass( "hover-active" );
  }
);

$( "#ph02" ).hover(
  function() {
    $( '#ph02a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph02a').removeClass( "hover-active" );
  }
);

$( "#ph03 " ).hover(
  function() {
    $( '#ph03a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph03a').removeClass( "hover-active" );
  }
);

$( "#ph04, #ph03b" ).hover(
  function() {
    $( '#ph04a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph04a').removeClass( "hover-active" );
  }
);

$( "#ph05" ).hover(
  function() {
    $( '#ph05a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph05a').removeClass( "hover-active" );
  }
);

$( "#ph06" ).hover(
  function() {
    $( '#ph06a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph06a').removeClass( "hover-active" );
  }
);

$( "#ph07" ).hover(
  function() {
    $( '#ph07a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph07a').removeClass( "hover-active" );
  }
);

$( "#ph08" ).hover(
  function() {
    $( '#ph08a' ).addClass( "hover-active" );
  }, function() {
    $( '#ph08a').removeClass( "hover-active" );
  }
);





}(jQuery));