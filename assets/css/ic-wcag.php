<?php

/**
 * Plugin Name: WCAG
 * Description: Wsparcie dla WCAG
 * Author: Paweł Kaczorowski
 * Author URI: mailto:p.kaczorowski83@gmail.com
 * Version: 1.0.0
 * Domain Path: /languages
 * Text Domain: ic-wcag
 */

namespace IC\Plugin\Wcag;

class Plugin {
	/** @var string */
	private $dir;

	/** @var string */
	private $file;

	/** @var string */
	private $basename;

	/**
	 * Constructor.
	 *
	 * @param string $file
	 */
	public function __construct( $file ) {
		$this->file     = $file;
		$this->dir      = dirname( $file );
		$this->basename = plugin_basename( $file );

		add_shortcode( 'ic_wcag', [ $this, 'shortcode' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'wp_enqueue_scripts' ], 0 );

		//Settings
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_action( 'admin_notices', [ $this, 'admin_notices' ] );
		add_action( 'admin_post_ic-settings-wcag', [ $this, 'save_settings' ] );
	}


	public function admin_notices() {
		if ( $notice = get_user_meta( get_current_user_id(), 'notice', 1 ) ) {
			delete_user_meta( get_current_user_id(), 'notice' );
			echo '<div class="notice notice-' . esc_attr( $notice['status'] ) . '"><p>' . $notice['message'] . '</p></div>';
		}
	}

	/**
	 * Add admin menu new item
	 */
	public function admin_menu() {
		add_options_page( 'Ustawienia WCAG', 'WCAG', 'manage_options', 'ic-settings-wcag', [ $this, 'settings_page' ] );
	}

	public function settings_page() {
		require $this->get_plugin_path( 'inc/views/settings-page.php' );
	}

	/**
	 * Save color settings
	 */
	public function save_settings() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'Sorry, you are not allowed to do that.' ) );
		}

		check_admin_referer( 'ic-settings-wcag' );

		update_option( 'wcag_voice', isset( $_POST['voice'] ) );
		update_option( 'wcag_contrast', isset( $_POST['contrast'] ) );
		update_option( 'wcag_font_size', isset( $_POST['font_size'] ) );

		$this->redirect( 'success', 'Ustawienia zostały zapisane' );
	}

	/**
	 * @param string $status
	 * @param string $message
	 * @param null   $url
	 */
	private function redirect( $status, $message, $url = null ) {
		update_user_meta( get_current_user_id(), 'notice', [
			'status'  => $status,
			'message' => $message,
		] );

		if ( $url === null ) {
			$url = wp_get_referer();
		}

		wp_redirect( $url );
		die();
	}

	public function shortcode() {
		if ( ! get_option( 'wcag_contrast' ) && ! get_option( 'wcag_font_size' ) ) {
			return '';
		}

		$output = '<div class="wcag-widget">';

		if ( get_option( 'wcag_font_size' ) ) {
				$output .= '<a href="#0" title="Zmień rozmiar tekstu" class="wcag-font-size wcag-font-size-level-0 js--change-font-size" data-level="0">A</a>';
				$output .= '<a href="#0" title="Zmień rozmiar tekstu" class="wcag-font-size wcag-font-size-level-1 js--change-font-size" data-level="1">A</a>';
				$output .= '<a href="#0" title="Zmień rozmiar tekstu" class="wcag-font-size wcag-font-size-level-2 js--change-font-size" data-level="2">A</a>';
		}

		if ( get_option( 'wcag_contrast' ) ) {
			$output .= '<a href="#0" id="wcag-contrast" title="Zmień kontrast" class="wcag-contrast-button js--change-contrast"></a>';
		}

		$output .= '</div>';

		return $output;
	}

	/**
	 * Register scripts and styles
	 */
	public function wp_enqueue_scripts() {
		//Speech Input
		wp_register_style( 'speech-input', $this->get_plugin_url( 'assets/vendor/speech-input/speech-input.css' ), [], '1.0.0' );
		wp_register_script( 'speech-input', $this->get_plugin_url( 'assets/vendor/speech-input/speech-input.js' ), [ 'jquery' ], '1.0.0', true );

		//jQuery Cookie
		wp_register_script( 'jquery-cookie', $this->get_plugin_url( 'assets/vendor/jquery-cookie/jquery.cookie.min.js' ), [ 'jquery' ], '1.4.1', true );

		//Style
		$style_file = 'assets/css/style.css';
		$style_ver  = filemtime( $this->get_plugin_path( $style_file ) );

		//Script
		$script_file = 'assets/js/wcag.js';
		$script_ver  = filemtime( $this->get_plugin_path( $script_file ) );

		$deps_js  = [ 'jquery-cookie' ];
		$deps_css = [];

		$wcag_voice = (bool) get_option( 'wcag_voice' );

		if ( $wcag_voice ) {
			$deps_css[] = 'speech-input';
		}

		wp_enqueue_style( 'ic-wcag', $this->get_plugin_url( $style_file ), $deps_css, $style_ver );
		wp_enqueue_script( 'ic-wcag', $this->get_plugin_url( $script_file ), $deps_js, $script_ver, 1 );
		wp_localize_script( 'ic-wcag', '__jsVars', [
			'talk_msg' => 'Powiedz coś...',
		] );

		if ( $wcag_voice ) {
			wp_enqueue_script( 'speech-input' );
		}
	}

	/**
	 * Get plugin url to file
	 *
	 * @param string $file
	 *
	 * @return string
	 */
	public function get_plugin_url( $file = '' ) {
		return plugins_url( $file, $this->file );
	}

	/**
	 * Get plugin path
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	public function get_plugin_path( $path = '' ) {
		return wp_normalize_path( plugin_dir_path( $this->file ) . '/' . $path );
	}
}

new Plugin( __FILE__ );