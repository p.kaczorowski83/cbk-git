<?php
/**
 * ===============================
 * TEMPLATE-PAGE-SPONSOR-DOWNLOAD.PHP - template for download file in sponsor page
 * ===============================
 *
 * Template name: Dokumenty do pobrania
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'sponsor-download' );
	get_template_part( 'template-parts/partial', 'menu-sponsor' );
	?>

</main>

<?php
get_footer();
