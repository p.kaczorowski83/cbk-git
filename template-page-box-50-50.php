<?php
/**
 * ===============================
 * TEMPLATE-PAGE-BOX-50-50.PHP - template for page with boxes 50/50
 * ===============================
 *
 * Template name: Boksy 50/50
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'box-50-50' );
	get_template_part( 'template-parts/partial', 'info-box' );
	if (is_page(array('9','303'))) { get_template_part( 'template-parts/partial', 'menu-patient' ); }
	?>

</main>

<?php
get_footer();
