<?php
/**
 * ===============================
 * TEMPLATE-PAGE-ABOUT.PHP - template for doctor page - conducted research
 * ===============================
 *
 * Template name: Prowadzaone badania
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'doctor-research-list' );
	get_template_part( 'template-parts/partial', 'menu-doctor' );
	?>

</main>

<?php
get_footer();
