<?php
/**
 * ===============================
 * TEMPLATE-PAGE-SPONSOR-EQUIPMENT.PHP - template for equipment in sponsor page
 * ===============================
 *
 * Template name: Galeria
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'gallery-small' );
	get_template_part( 'template-parts/partial', 'menu-sponsor' );
	?>

</main>

<?php
get_footer();
