<?php
/**
 * ===============================
 * TEMPLATE-PAGE-ABOUT.PHP - template for doctor page
 * ===============================
 *
 * Template name: Dla lekarzy
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'doctor-home' );
	?>

</main>

<?php
get_footer();
