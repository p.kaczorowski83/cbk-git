<?php

/**
 * NID functions and definitions
 *
 * @package Nid theme
 * @since 1.0
 */

/**
 * NID template was created on Wordpress 4.7, before update core of Wordpress please check how it work in development environment
 */
function nid_theme_setup() {
    /**
     * Register places for menus in theme
     */
    register_nav_menus(array(
        'top' => __('Top Menu', 'nid'),
        'homepageTools' => __('Homepage Tools', 'nid'),
        'social' => __('Social Links Menu', 'nid'),
        'footer' => __('Footer Menu', 'nid'),
        'partners' => __('Partners Links Menu', 'nid'),
    ));

    load_theme_textdomain( 'nid', get_template_directory() . '/languages' );
    
    add_image_size( 'gallery_size', 290, 180, true );
    
    remove_shortcode('gallery');
    add_shortcode('gallery', 'nid_gallery_shortcode');

}

add_action('after_setup_theme', 'nid_theme_setup');

add_theme_support( 'title-tag' );

add_filter( 'image_size_names_choose', 'my_custom_sizes' );
 
function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'gallery_size' => __( 'Galeria' ),
    ) );
}

/**
 * Register widgets in theme
 */
function nid_theme_widgets() {
    register_sidebar(array(
        'name' => __('Sidebar', 'nid'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'nid'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => __('Footer Contact', 'nid'),
        'id' => 'sidebar-2',
        'description' => __('Add widgets here to appear in your  right side of footer.', 'nid'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => __('Knowledge base', 'nid'),
        'id' => 'knowledge-base',
        'description' => __('Add widgets here to appear in knowledge base in homepage.', 'nid'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    ));
    register_sidebar(array(
        'name' => __('Paths homepage', 'nid'),
        'id' => 'paths',
        'description' => __('Add widgets here to appear in paths secton in homepage.', 'nid'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    ));
    register_sidebar(array(
        'name' => __('Oferta NID dla samorządów', 'nid'),
        'id' => 'oferty-nid',
        'description' => __('Add widgets here to appear in paths secton in homepage.', 'nid'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    ));
}

add_action('widgets_init', 'nid_theme_widgets');

/**
 * Get link to image by key
 *
 * @param $key string key or filename
 * @param $justEcho bool
 *
 * @return string
 */
function getImage($key, $justEcho = true) {
    $baseDir = esc_url(home_url('/')) . 'wp-content/themes/nid/assets/images/';
    $availableImages = [
        'image-not-found' => 'NoPicAvailable.png',
        'logo' => 'logo_nid_new.svg',
        'a-' => 'a-.svg',
        'a+' => 'a+.svg',
        'good-practices' => 'dobre-praktyki.svg',
        'facebook' => 'fb.svg',
        'wallet' => 'finanse.svg',
        'contrast' => 'kontrast.svg',
        'tools' => 'narzedzia.svg',
        'law' => 'prawo.svg',
        'publish' => 'publikacje.svg',
        'paths' => 'sciezki-dzialania.svg',
        'social' => 'spolecznosc.svg',
        'arrow' => 'strzalka.svg',
        'search' => 'szukaj.svg',
        'youtube' => 'yb.svg',
        'monuments' => 'zabytki-w-gminie.svg',
        'management' => 'zarzadzanie-i-rozwoj.svg',
        'see-all-path' => 'zobacz-wszystkie-sciezki.svg',
    ];
    if (!key_exists($key, $availableImages)) {
        if (file_exists(__DIR__ . 'assets/images/' . $key)) {
            return $baseDir . $key;
        }

        $key = 'image-not-found';
    }

    if ($justEcho) {
        echo $baseDir . $availableImages[$key];
    } else {
        return $baseDir . $availableImages[$key];
    }
}

/**
 * Turn off admin bar at frontend
 * for development, can be turned on at production environment
 */
add_filter('show_admin_bar', '__return_false');

/**
 * @param $locationKey string
 * @param $baseClass string
 * @return array|false
 */
function nid_theme_get_menu_items($locationKey, $baseClass = '', $limit = 100000, $params = array()) {
    if (($locations = get_nav_menu_locations()) && isset($locations[$locationKey])) {
        $active_id = array();
        if (isset($params['check_active'])) {
            if (is_page()) {
                $page = get_page(get_the_ID());
                while ($page->post_parent)
                    $page = get_page($page->post_parent);
                $active_id = array($page->ID);
            } else if (is_archive() || is_single()) {
                $active_id = array(get_query_var('post_type'));
            }
        }
        $menu = wp_get_nav_menu_object($locations[$locationKey]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $ulClass = (empty($baseClass)) ? '' : $baseClass . '-list';
        $liClass = (empty($baseClass)) ? '' : $baseClass . '-item';
        $aClass = (empty($baseClass)) ? '' : $baseClass . '-href';

        $menu_list = "\t\t\t\t" . "<ul class='$ulClass'>" . "\n";
        foreach ((array) $menu_items as $key => $menu_item) {
            if ($key + 1 > $limit) {
                break;
            }
            $title = $menu_item->title;
            $url = $menu_item->url;

            $extraClass = '';
            foreach ($menu_item->classes as $class) {
                $extraClass .= ' ' . $class;
            }
            if (in_array($menu_item->object_id, $active_id) || in_array($menu_item->object, $active_id))
                $extraClass .= ' active';
            
            $target = $menu_item->target ? $menu_item->target : (isset($params['target']) ? $params['target'] : '' );
            $menu_list .= "\t\t\t\t\t" . "<li class='$liClass $extraClass'><a class='$aClass ";
            $menu_list .= '\' href="' . $url . '" target="' . $target . '"><p class="' . $baseClass . '--title">' . $title . '</p>';
            if (isset($params['show_desc']) && isset($menu_item->post_content)) {
                $menu_list .= '<p class="' . $baseClass . '--description">' . $menu_item->post_content . '</p>';
            }
            $menu_list .= '</a></li>' . "\n";
        }
        $menu_list .= "\t\t\t\t" . '</ul>' . "\n";

    } else {
        $menu_list = '<!-- no list defined -->';
    }
    echo $menu_list;
}

/**
 * Showing breadcrumbs
 * @param $breadrumbs array
 *
 */
function showMyBreadcrumbs($breadcrumbs) {
    echo "<a href='" . get_home_url() . "'>Strona główna</a>";
    if (!is_array($breadcrumbs)) {
        return;
    }
    $total = count($breadcrumbs) - 1;
    foreach ($breadcrumbs as $key => $row) {
        if (isset($row['title'])) {
            $url = (isset($row['url'])) ? $row['url'] : '#';
            if ($total == $key) {
                $title = '<b>' . $row['title'] . '</b>';
            } else {
                $title = $row['title'];
            }
            echo ' / <a href="' . $url . '">' . $title . '</a>';
        }
    }
}

/*
  function nid_theme_scripts()
  {
  $ajax_url = admin_url('admin-ajax.php');
  wp_enqueue_script('inv-paths-creator', get_template_directory_uri() . '/assets/js/inv_paths_creator.js', array('jquery'), '1.0.0', true);
  wp_localize_script('inv-paths-creator', 'ajax_object', array('ajax_url' => $ajax_url));
  wp_enqueue_script('nid-contact-form', get_template_directory_uri() . '/assets/js/contact_form.js', array('jquery'), '1.0.0', true);
  wp_localize_script('nid-contact-form', 'ajax_object', array('ajax_url' => $ajax_url));
  wp_enqueue_script('nid-tags-form', get_template_directory_uri() . '/assets/js/nid_tags_form.js', array('jquery'), '1.0.0', true);
  wp_localize_script('nid-tags-form', 'ajax_object', array('ajax_url' => $ajax_url));
  }

  add_action('wp_enqueue_scripts', 'nid_theme_scripts');
 */

function nid_contact_form_send() {
    $errors = array();
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([ 'secret' => '6LdJmiYUAAAAAN7-_J6EasKUSismfgnOQMYPnxyU', 'response' => $_POST['g-recaptcha-response']]));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);
    $resp = json_decode($server_output);
    if (!$resp || $resp->success === false) {
        $errors[] = array('field' => 'reCaptcha', 'type' => 1);
        $error_message = __('Uzupełnij reCAPTCHA', 'nid');
    } else {
        $fields = array(
            'email' => 'E-mail',
            'stanowisko' => 'Stanowisko',
            'gmina' => 'Gmina',
            'tekst' => 'Wysłała następującą wiadomość',
            'zgoda' => 'Zgoda'
        );
        $values = array();
        foreach ($fields as $field => $val) {
            if (!isset($_POST[$field]) || !$_POST[$field])
                $errors[] = array('field' => $field, 'type' => 1);
            else
                $values[$field] = $_POST[$field];
        }
        $error_message = __('Wypełnij poprawnie zaznaczone pola.', 'nid');
    }

    if (empty($errors)) {
        $to = get_option('nid_form_email');
        if (!$to)
            $to = 'lofiara@nid.pl';
        $subject = 'Wiadomość z formularza kontaktowego';
        $message = '<div>Osoba legitymująca się następującymi danymi:<br /><ul>';
        $message .= '<li>' . $fields['email'] . ': ' . $values['email'] . '</li>';
        $message .= '<li>' . $fields['gmina'] . ': ' . $values['gmina'] . '</li>';
        $message .= '<li>' . $fields['stanowisko'] . ': ' . $values['stanowisko'] . '</li>';
        $message .= '</ul><br />' . $fields['tekst'] . ':<br /><br />';
        $message .= '<div>' . $values['tekst'] . '</div>';
        $message .= '</div>';
        $headers = 'Content-Type: text/html';
        try {
            $mail = wp_mail($to, $subject, $message, $headers);
        } catch (Exception $e) {
            $mail = $e->getMessage();
        }
        echo json_encode(array('status' => true, 'message' => __('Twoja wiadomość została wysłana.', 'nid'), 'mail' => $mail));
    } else {
        echo json_encode(array('status' => false, 'message' => $error_message, 'errors' => $errors));
    }
    wp_die();
}

add_action('wp_ajax_nid_contact_form_send', 'nid_contact_form_send');
add_action('wp_ajax_nopriv_nid_contact_form_send', 'nid_contact_form_send');

function nid_autocomplete_search() {
    $args = array(
        'posts_per_page' => 10,
        'post_type' => array('baza_wiedzy', 'sciezki-dzialania', 'dobre-praktyki', 'narzedzia', 'akty-prawne', 'publikacje'),
        'post_status' => 'publish',
        's' => $_GET['s']
    );
    $posts = get_posts($args);
    $posts_data = array();
    foreach ($posts as $key => $post) {
        $post_data[$key]['value'] = $post->post_title;
        $post_data[$key]['url'] = get_permalink($post->ID);
    }
    echo json_encode($post_data);
    wp_die();
}

add_action('wp_ajax_nid_autocomplete_search', 'nid_autocomplete_search');
add_action('wp_ajax_nopriv_nid_autocomplete_search', 'nid_autocomplete_search');

function nid_ajax_request_for_content_only() {
    $templates = array('archive-akty-prawne', 'archive-dobre-praktyki', 'archive-narzedzia', 'archive-oferty', 'archive-publikacje', 'archive', 'category', 'page-baza-wiedzy', 'page-dziedzictwo', 'page-finanse', 'page-prawo', 'page-sciezki', 'page-spolecznosc', 'page-zarzadzanie-i-rozwoj', 'search');
    if (isset($_POST['template']) && in_array($_POST['template'], $templates)) {
        $ajax_request_for_content_only = true;
        ob_start();
        include(locate_template($_POST['template'] . '.php'));
        $data = ob_get_clean();
        echo json_encode(array('status' => true, 'data' => $data, 'results' => isset($results) ? $results : ''));
    } else
        echo json_encode(array('status' => false));
    wp_die();
}

add_action('wp_ajax_nid_ajax_request_for_content_only', 'nid_ajax_request_for_content_only');
add_action('wp_ajax_nopriv_nid_ajax_request_for_content_only', 'nid_ajax_request_for_content_only');

function nid_get_filtered_sorted_paginated_posts($params = array()) {
    $args = array(
        'nopaging' => true,
        'order' => 'DESC',
        'orderby' => 'modified',
        'post_status' => 'publish'
    );
    $pagenum = isset($_GET['pagenum']) && $_GET['pagenum'] ? $_GET['pagenum'] : 1;
    $posts_per_page = isset($params['posts_per_page']) && $params['posts_per_page'] ? $params['posts_per_page'] : 7;
    if (isset($params['limit']) && $params['limit']) {
        $args['posts_per_page'] = $params['limit'];
        $args['nopaging'] = false;
    }
    if (isset($params['post_type']) && $params['post_type']) {
        $args['post_type'] = $params['post_type'];
    }
    if (isset($params['category']) && $params['category']) {
        if (is_array($params['category'])) {
            $args['cat'] = implode(',', $params['category']);
        } else {
            $args['cat'] = $params['category'];
        }
    }
    if (isset($_REQUEST['s']) && $_REQUEST['s'])
        $args['s'] = $_REQUEST['s'];
    else if (isset($_REQUEST['order']) && $_REQUEST['order'] && $_REQUEST['order'] != 'useful') {
        $args['orderby'] = $_REQUEST['order'];
        if ($_REQUEST['order'] == 'title')
            $args['order'] = 'ASC';
    } else if (isset($params['orderby']) && $params['orderby'] && $params['orderby'] != 'useful') {
        $args['orderby'] = $params['orderby'];
    } else {
        $default_order = get_option('nid_theme_order');
        $_GET['order'] = $_REQUEST['order'] = $default_order ? $default_order : 'useful';
        $args['orderby'] = $default_order && $default_order != 'useful' ? $default_order : 'modified';
    }
    if ($args['orderby'] == 'title')
        $args['order'] = 'ASC';
    if (isset($_REQUEST['selected_tags']) && $_REQUEST['selected_tags']) {
        $selected_tags = explode(',', $_REQUEST['selected_tags']);
        if ($selected_tags && !empty($selected_tags)) {
            if (isset($params['post_type']) && $params['post_type']) {
                $args['tax_query'] = array();
                if (is_array($params['post_type'])) {
                    foreach ($params['post_type'] as $post_type) {
                        $taxonomy = str_replace('-', '_', $post_type) . '_tag';
                        $tags_from_taxonomy = array();
                        foreach (get_terms(array(
                            'taxonomy' => $taxonomy,
                            'include' => $selected_tags
                        )) as $term) {
                            $tags_from_taxonomy[] = $term->term_id;
                        }
                        if (!empty($tags_from_taxonomy)) {
                            $args['tax_query'][] = array(
                                'taxonomy' => $taxonomy,
                                'terms' => $tags_from_taxonomy,
                                'operator' => 'AND'
                            );
                        }
                    }
//                    $args['tax_query']['relation'] = 'OR';
                } else
                    $args['tax_query'][] = array(
                        'taxonomy' => str_replace('-', '_', $params['post_type']) . '_tag',
                        'terms' => $selected_tags,
                        'operator' => 'AND'
                    );
            } else
                $args['tag__and'] = $selected_tags;
        }
    } else if (isset($params['tag_slug']) && $params['tag_slug']) {
        if (isset($params['post_type']) && $params['post_type']) {
            $args['tax_query'] = array();
            if (is_array($params['post_type'])) {
                foreach ($params['post_type'] as $post_type) {
                    $taxonomy = str_replace('-', '_', $post_type) . '_tag';
                    $args['tax_query'][] = array(
                        'taxonomy' => $taxonomy,
                        'terms' => $params['tag_slug'],
                        'field' => 'slug'
                    );
                }
                $args['tax_query']['relation'] = 'OR';
            } else
                $args['tax_query'][] = array(
                    'taxonomy' => str_replace('-', '_', $params['post_type']) . '_tag',
                    'terms' => $params['tag_slug'],
                    'field' => 'slug'
                );
        } else
            $args['tag'] = $params['tag_slug'];
    }
    $wp_query = new WP_Query($args);

    if (isset($_REQUEST['order']) && $_REQUEST['order'] == 'useful' && function_exists('inv_useful_posts_get_posts')) {
        $useful_posts = inv_useful_posts_get_posts();
        arsort($useful_posts);
        $posts_to_sort = array();
        $posts_sorted = array();
        foreach ($wp_query->posts as $post)
            $posts_to_sort[$post->ID] = $post;
        foreach ($useful_posts as $key => $useful) {
            if (isset($posts_to_sort[$key])) {
                $posts_sorted[] = $posts_to_sort[$key];
                unset($posts_to_sort[$key]);
            }
        }
        foreach ($posts_to_sort as $post)
            $posts_sorted[] = $post;
    }
    else
        $posts_sorted = $wp_query->posts;

    return array(array_slice($posts_sorted, ($pagenum - 1) * $posts_per_page, $posts_per_page), array(
            'count' => count($posts_sorted),
            'page' => $pagenum,
            'paths_per_page' => $posts_per_page,
            's' => isset($_REQUEST['s']) ? $_REQUEST['s'] : null,
            'order' => isset($_REQUEST['order']) ? $_REQUEST['order'] : null,
            'tags' => isset($_REQUEST['selected_tags']) ? $_REQUEST['selected_tags'] : null
    ));
}

function nid_transform_content($content, $container = '|') {
    preg_match('/<h\d.*\/h\d>/', $content, $h);
    if (!empty($h))
        return str_replace('|', strip_tags($h[0]), $container);
    $content = trim(strip_tags(strip_shortcodes($content)));
    if (!$content)
        return '';
    if (mb_strlen($content) > 200)
        $content = mb_substr($content, 0, 200) . '...';
    return str_replace('|', $content, $container);
}

function nid_find_url_in_post_content($post) {
    if (function_exists('get_field') && $url = get_field('redirect_url', $post->ID))
        return $url;
    preg_match('/https?[0-9a-zA-Z\\\.\?=&\+-_%#]*\s?/', $post->post_content, $h);
    if (!empty($h))
        return $h[0];
    return get_permalink($post);
}

function nid_get_custom_post_type_url($post) {
    $target = '';
    if ($post->post_type == 'sciezki-dzialania')
        $url = get_page_link(PATHS_CREATOR_PATH_PAGE_ID) . '?path=' . $post->ID;
    else if ($post->post_type == 'akty-prawne') {
        $url = nid_find_url_in_post_content($post);
        $target = 'target="_blank"';
    } else if ($post->post_type == 'oferty' && function_exists('get_field') && $url = get_field('redirect_url', $post->ID)) {
        $target = 'target="_blank"';
    } else
        $url = get_permalink($post);
    return array($url, $target);
}

function nid_get_related_posts($post_id) {
    $main_post_types = array(
        'baza_wiedzy' => 0,
        'sciezki-dzialania' => 0,
        'dobre-praktyki' => 0,
        'narzedzia' => 0,
        'akty-prawne' => 0
    );
    $relations = CustomRelatedPosts::get()->relations_to($post_id);
    $post_type = get_post_type($post_id);
    foreach ($relations as &$relation) {
        $related_post_type = get_post_type($relation['id']);
        if (array_key_exists($related_post_type, $main_post_types)) {
            $relation['post_type'] = $related_post_type;
            $post = get_post($relation['id']);
            $relation['desc'] = nid_transform_content($post->post_content);
            $main_post_types[$related_post_type]++;
            if ($related_post_type == 'sciezki-dzialania')
                $relation['permalink'] = get_page_link(PATHS_CREATOR_PATH_PAGE_ID) . '?path=' . $relation['id'];
            if ($related_post_type == 'akty-prawne') {
                $relation['permalink'] = nid_find_url_in_post_content($post);
                $relation['target'] = ' target="blank" ';
            }
        }
    }
    return array($main_post_types, $post_type, $relations);
}

function nid_get_related_posts_by_categories($post_id) {
    $main_categories = array(
        3 => 0, //ścieżki działania
        4 => 0,
        5 => 0,
        11 => 0,
        12 => 0
    );
    $relations = CustomRelatedPosts::get()->relations_to($post_id);
    $post_categories = get_the_category($post_id);
    if (!empty($post_categories)) {
        $tmp_post_category = $post_categories[0];
        while ($tmp_post_category->parent != 0) {
            $tmp_post_category = get_term($tmp_post_category->parent);
        }
        $post_category = $tmp_post_category->term_id;
    }
    else
        $post_category = 0;
    foreach ($relations as &$relation) {
        $categories = get_the_category($relation['id']);
        if (!empty($categories)) {
            $category = $categories[0]->parent ? $categories[0]->parent : $categories[0]->term_id;
            if (array_key_exists($category, $main_categories)) {
                $relation['category'] = $category;
                $post = get_post($relation['id']);
                $relation['desc'] = nid_transform_content($post->post_content);
                $main_categories[$category]++;
                if ($category == 3)
                    $relation['permalink'] = get_page_link(PATHS_CREATOR_PATH_PAGE_ID) . '?path=' . $relation['id'];
            }
        }
    }
    return array($main_categories, $post_category, $relations);
}

function nid_get_publication_image($post_id) {
    $images = get_attached_media('image', $post_id);
    if (!empty($images))
        return current($images);
    return false;
}

function nid_get_publication_cover_url($post_id) {
    if (function_exists('get_field') && $image = get_field('publication_cover')) {
        return $image['sizes']['medium'];
    } else if (isset($_POST['action']) && $_POST['action'] == 'nid_ajax_request_for_content_only') {
        $image = get_post_meta($post_id, 'publication_cover');
        $src = wp_get_attachment_image_src($image[0],'medium');
        return $src[0];
    } else if ($image = nid_get_publication_image($post_id)) {
        $src = wp_get_attachment_image_src($image->ID,'medium');
        return $src[0];
    }
    return false;
}

add_action('init', 'inv_tinymce_plugin_init');

function inv_tinymce_plugin_init() {
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
        return;
    add_filter("mce_external_plugins", "inv_tinymce_plugin_register");
    add_filter('mce_buttons', 'inv_tinymce_plugin_add_button');
}

function inv_tinymce_plugin_register($plugin_array) {
    $plugin_array['inv_tinymce_remember_button'] = get_template_directory_uri() . '/assets/js/inv_tinymce_remember_plugin.js';
    $plugin_array['inv_tinymce_link_button'] = get_template_directory_uri() . '/assets/js/inv_tinymce_link_plugin.js';
    return $plugin_array;
}

function inv_tinymce_plugin_add_button($buttons) {
    $buttons[] = "inv_tinymce_remember_button";
    $buttons[] = "inv_tinymce_link_button";
    return $buttons;
}

add_shortcode('inv_tinymce_remember', 'inv_tinymce_remember');

function inv_tinymce_remember($args = array()) {
    $icons = array(
        'pin' => 'Zapamiętaj',
        'owl' => 'Porada eksperta',
        'bulb' => 'Warto wiedzieć',
        'attention' => 'Uwaga',
    );
    $icon = isset($args['icon']) ? $args['icon'] : 'pin';
    return '<div class="remember" style="padding: 10px 30px 20px 0; font-size: 0.9em; text-align: justify;">' .
            '<div aria-hidden=”true” class="remember-icon-' . $icon . '" title="' . (isset($icons[$icon]) ? $icons[$icon] : $icons['pin']) . '"></div>' .
            '<div class="remember-text">' . (isset($args['val']) ? $args['val'] : '') . '</div>' .
            '</div>';
}

// Register Custom Post Type
function nid_custom_post_type() {

    $labels = array(
        'name' => __('Baza Wiedzy', 'nid'),
        'singular_name' => __('Baza Wiedzy', 'nid'),
        'menu_name' => __('Baza Wiedzy', 'nid'),
        'name_admin_bar' => __('Baza Wiedzy', 'nid'),
        'all_items' => __('Wszystkie wpisy', 'nid'),
    );
    $args = array(
        'label' => __('Baza Wiedzy', 'nid'),
        'labels' => $labels,
        'supports' => array(),
        'taxonomies' => array('category'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type('baza_wiedzy', $args);

    $args['taxonomies'] = array();

    $labels = array(
        'name' => __('Ścieżki działania', 'nid'),
        'singular_name' => __('Ścieżki działania', 'nid'),
        'menu_name' => __('Ścieżki działania', 'nid'),
        'name_admin_bar' => __('Ścieżki działania', 'nid'),
        'all_items' => __('Wszystkie ścieżki', 'nid'),
    );
    $args['label'] = __('Ścieżki działania', 'nid');
    $args['labels'] = $labels;
    $args['menu_position'] = 6;
    register_post_type('sciezki-dzialania', $args);

    $labels = array(
        'name' => __('Kroki', 'nid'),
        'singular_name' => __('Kroki', 'nid'),
        'menu_name' => __('Kroki', 'nid'),
        'name_admin_bar' => __('Kroki', 'nid'),
        'all_items' => __('Wszystkie kroki', 'nid'),
    );
    $args['label'] = __('Kroki', 'nid');
    $args['labels'] = $labels;
    $args['exclude_from_search'] = true;
    $args['menu_position'] = 7;
    register_post_type('kroki', $args);

    $args['exclude_from_search'] = false;

    $labels = array(
        'name' => __('Dobre Praktyki', 'nid'),
        'singular_name' => __('Dobre Praktyki', 'nid'),
        'menu_name' => __('Dobre Praktyki', 'nid'),
        'name_admin_bar' => __('Dobre Praktyki', 'nid'),
        'all_items' => __('Wszystkie wpisy', 'nid'),
    );
    $args['label'] = __('Dobre Praktyki', 'nid');
    $args['labels'] = $labels;
    $args['menu_position'] = 8;
    register_post_type('dobre-praktyki', $args);

    $labels = array(
        'name' => __('Narzędzia', 'nid'),
        'singular_name' => __('Narzędzia', 'nid'),
        'menu_name' => __('Narzędzia', 'nid'),
        'name_admin_bar' => __('Narzędzia', 'nid'),
        'all_items' => __('Wszystkie wpisy', 'nid'),
    );
    $args['label'] = __('Narzędzia', 'nid');
    $args['labels'] = $labels;
    $args['menu_position'] = 9;
    register_post_type('narzedzia', $args);

    $labels = array(
        'name' => __('Publikacje', 'nid'),
        'singular_name' => __('Publikacje', 'nid'),
        'menu_name' => __('Publikacje', 'nid'),
        'name_admin_bar' => __('Publikacje', 'nid'),
        'all_items' => __('Wszystkie wpisy', 'nid'),
    );
    $args['label'] = __('Publikacje', 'nid');
    $args['labels'] = $labels;
    $args['menu_position'] = 10;
    register_post_type('publikacje', $args);

    $labels = array(
        'name' => __('Akty Prawne', 'nid'),
        'singular_name' => __('Akty Prawne', 'nid'),
        'menu_name' => __('Akty Prawne', 'nid'),
        'name_admin_bar' => __('Akty Prawne', 'nid'),
        'all_items' => __('Wszystkie wpisy', 'nid'),
    );
    $args['label'] = __('Akty Prawne', 'nid');
    $args['labels'] = $labels;
    $args['menu_position'] = 11;
    register_post_type('akty-prawne', $args);
    
    $labels = array(
        'name' => __('Oferta NID', 'nid'),
        'singular_name' => __('Oferta NID', 'nid'),
        'menu_name' => __('Oferta NID', 'nid'),
        'name_admin_bar' => __('Oferta NID', 'nid'),
        'all_items' => __('Wszystkie oferty', 'nid'),
    );
    $args['label'] = __('Oferta NID', 'nid');
    $args['labels'] = $labels;
    $args['taxonomies'] = array('category');
    $args['menu_position'] = 12;
    register_post_type('oferty', $args);
}

add_action('init', 'nid_custom_post_type', 0);

function nid_custom_taxonomy() {

    $args = array(
        'labels' => array(),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
    );
    register_taxonomy('baza_wiedzy_tag', array('baza_wiedzy'), $args);
    register_taxonomy('sciezki_dzialania_tag', array('sciezki-dzialania'), $args);
    register_taxonomy('dobre_praktyki_tag', array('dobre-praktyki'), $args);
    register_taxonomy('narzedzia_tag', array('narzedzia'), $args);
    register_taxonomy('publikacje_tag', array('publikacje'), $args);
    register_taxonomy('akty_prawne_tag', array('akty-prawne'), $args);
    register_taxonomy('oferty_tag', array('oferty'), $args);
}

add_action('init', 'nid_custom_taxonomy', 0);

function nid_antybot_email_shortcode($params, $email = null) {
    if (!is_email($email)) {
        return $email;
    }
    return '<a href="mailto:' . antispambot($email) . '">' . antispambot($email) . '</a>';
}

add_shortcode('nid_antybot_email', 'nid_antybot_email_shortcode');
add_filter('widget_text', 'do_shortcode');

/**
 * Builds the Gallery shortcode output.
 *
 * This implements the functionality of the Gallery Shortcode for displaying
 * WordPress images on a post.
 *
 * @since 2.5.0
 *
 * @staticvar int $instance
 *
 * @param array $attr {
 *     Attributes of the gallery shortcode.
 *
 *     @type string       $order      Order of the images in the gallery. Default 'ASC'. Accepts 'ASC', 'DESC'.
 *     @type string       $orderby    The field to use when ordering the images. Default 'menu_order ID'.
 *                                    Accepts any valid SQL ORDERBY statement.
 *     @type int          $id         Post ID.
 *     @type string       $itemtag    HTML tag to use for each image in the gallery.
 *                                    Default 'dl', or 'figure' when the theme registers HTML5 gallery support.
 *     @type string       $icontag    HTML tag to use for each image's icon.
 *                                    Default 'dt', or 'div' when the theme registers HTML5 gallery support.
 *     @type string       $captiontag HTML tag to use for each image's caption.
 *                                    Default 'dd', or 'figcaption' when the theme registers HTML5 gallery support.
 *     @type int          $columns    Number of columns of images to display. Default 3.
 *     @type string|array $size       Size of the images to display. Accepts any valid image size, or an array of width
 *                                    and height values in pixels (in that order). Default 'thumbnail'.
 *     @type string       $ids        A comma-separated list of IDs of attachments to display. Default empty.
 *     @type string       $include    A comma-separated list of IDs of attachments to include. Default empty.
 *     @type string       $exclude    A comma-separated list of IDs of attachments to exclude. Default empty.
 *     @type string       $link       What to link each image to. Default empty (links to the attachment page).
 *                                    Accepts 'file', 'none'.
 * }
 * @return string HTML content to display gallery.
 */
function nid_gallery_shortcode( $attr ) {
	$post = get_post();
        
	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}

	/**
	 * Filters the default gallery shortcode output.
	 *
	 * If the filtered output isn't empty, it will be used instead of generating
	 * the default gallery template.
	 *
	 * @since 2.5.0
	 * @since 4.2.0 The `$instance` parameter was added.
	 *
	 * @see gallery_shortcode()
	 *
	 * @param string $output   The gallery output. Default empty.
	 * @param array  $attr     Attributes of the gallery shortcode.
	 * @param int    $instance Unique numeric ID of this gallery shortcode instance.
	 */
	$output = apply_filters( 'post_gallery', '', $attr, $instance );
	if ( $output != '' ) {
		return $output;
	}

	$html5 = current_theme_supports( 'html5', 'gallery' );
	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => $html5 ? 'figure'     : 'dl',
		'icontag'    => $html5 ? 'div'        : 'dt',
		'captiontag' => $html5 ? 'figcaption' : 'div',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => '',
		'link'       => ''
	), $attr, 'gallery' );

	$id = intval( $atts['id'] );

	if ( ! empty( $atts['include'] ) ) {
		$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( ! empty( $atts['exclude'] ) ) {
		$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	} else {
		$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	}

	if ( empty( $attachments ) ) {
		return '';
	}

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
		}
		return $output;
	}

	$itemtag = tag_escape( $atts['itemtag'] );
	$captiontag = tag_escape( $atts['captiontag'] );
	$icontag = tag_escape( $atts['icontag'] );
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) ) {
		$itemtag = 'dl';
	}
	if ( ! isset( $valid_tags[ $captiontag ] ) ) {
		$captiontag = 'dd';
	}
	if ( ! isset( $valid_tags[ $icontag ] ) ) {
		$icontag = 'dt';
	}

	$columns = intval( $atts['columns'] );
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = '';

	/**
	 * Filters whether to print default gallery styles.
	 *
	 * @since 3.1.0
	 *
	 * @param bool $print Whether to print default gallery styles.
	 *                    Defaults to false if the theme supports HTML5 galleries.
	 *                    Otherwise, defaults to true.
	 */
	if ( apply_filters( 'use_default_gallery_style', ! $html5 ) ) {
		$gallery_style = "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
				border: 2px solid #cfcfcf;
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
			/* see gallery_shortcode() in wp-includes/media.php */
		</style>\n\t\t";
	}

	$size_class = sanitize_html_class( $atts['size'] );
	$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

	/**
	 * Filters the default gallery shortcode CSS styles.
	 *
	 * @since 2.5.0
	 *
	 * @param string $gallery_style Default CSS styles and opening HTML div container
	 *                              for the gallery shortcode output.
	 */
	$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {

		$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
		if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
			$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
		} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
			$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
		} else {
			$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
		}
		$image_meta  = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		}
		$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "
                    <{$icontag} class='gallery-icon {$orientation}'>
                            $image_output
                    ";
                    if ( $captiontag && trim($attachment->post_excerpt) ) {
                        $output .= "
                            <{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
                            " . wptexturize($attachment->post_excerpt) . "
                            </{$captiontag}>";
                    }
		$output .= "</{$icontag}></{$itemtag}>";
                $i++;
//		if ( ! $html5 && $columns > 0 && $i % $columns == 0 ) {
//			$output .= '<br style="clear: both" />';
//		}
	}

//	if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
	if ( ! $html5 && $columns > 0 ) {
		$output .= "
			<br style='clear: both' />";
	}

	$output .= "
		</div>\n";

	return $output;
}

add_action('admin_menu', 'nid_theme_settings_menu');

function nid_theme_settings_menu(){
    add_options_page('NID Theme - Ustawienia', 'NID Theme', 'manage_options', 'nid-theme-settings-page', 'nid_theme_settings');
}

function nid_theme_default_settings() {
    if ( !get_option('nid_theme_order') ) {
        update_option('nid_theme_order', 'useful');
    }
    if ( !get_option('nid_form_email') ) {
        update_option('nid_form_email', 'lofiara@nid.pl');
    }
}

function nid_theme_settings() {

    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    if (isset($_POST['submit-settings'])) {

        if (isset($_POST['nid_theme_order'])) {
            update_option('nid_theme_order', $_POST['nid_theme_order']);
        }
        if (isset($_POST['nid_form_email'])) {
            update_option('nid_form_email', $_POST['nid_form_email']);
        }

    }
    nid_theme_default_settings();
    $selected_order = get_option('nid_theme_order');
    $nid_form_email = get_option('nid_form_email');

    echo '<h2><strong style="font-size: 1.2em;">NID Theme - Ustawienia</strong></h2><br><br>';

    ?><form method="post" name="options" target="_self"><?php
        settings_fields('nid_theme_settings_group');
        ?><table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">Domyślne sortowanie</th>
                    <td>
                        <fieldset>
                            <label>
                                <select name="nid_theme_order">
                                    <option value="modified" <?php echo $selected_order == 'modified' ? 'selected="selected"' : ''; ?>>Po dacie publikacji</option>
                                    <option value="useful" <?php echo $selected_order == 'useful' ? 'selected="selected"' : ''; ?>>Po przydatności</option>
                                    <option value="title" <?php echo $selected_order == 'title' ? 'selected="selected"' : ''; ?>>Po nazwie</option>
                                </select>
                            </label><br>
                        </fieldset> 
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">E-mail do formularza</th>
                    <td>
                        <fieldset>
                            <label>
                                <input type="email" name="nid_form_email" value="<?php echo $nid_form_email; ?>" />
                            </label> - adres na który będą wysyłane wiadomości z furmularza kontaktowego<br>
                        </fieldset> 
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" class="button-primary" name="submit-settings" value="Zapisz" /></p>
    </form><?php
}

function nid_monuments_search() {
    $zabytek_api_url = 'https://api.zabytek.pl/';
    $zabytek_api_ver = 'v1_2/objects?';
    $commune = explode(';', addslashes(trim($_POST['commune'])));
    $params = array(
        'address_commune' => $commune[0],
        'address_county' => $commune[1],
        'address_region' => $commune[2],
        'token' => 'XQqErVuoinYgemuDwWOrNG2hoWxOJwmF',
        'output' => 'json',
//        'sort' => 'source desc',
        'sort' => 'address_city,address_street,address_number',
        'page_limit' => '6',
//        'inspire_id_duplicates_hide' => true,
//        'unique_geoportal' => 1
    );
    if (isset($_POST['page']))
        $params['page'] = $_POST['page'];
    $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    );  
//    if ($response = json_decode(file_get_contents($zabytek_api_url . $zabytek_api_ver . http_build_query($params)))) {
    if ($response = json_decode(file_get_contents($zabytek_api_url . $zabytek_api_ver . http_build_query($params), false, stream_context_create($arrContextOptions)))) {
        $html = $name = '';
        $count = 0;
        $collecion = array(
            'unesco' => 'Zabytek nieruchomy',
            'pomniki-historii' => 'Zabytek nieruchomy',
            'zabytki-nieruchome' => 'Zabytek nieruchomy',
            'stanowiska-archeologiczne' => 'Stanowisko archeologiczne'
        );
        $protection_type = array(
            'unesco' => 'światowe dziedzictwo UNESCO',
            'pomniki-historii' => 'Pomnik historii',
            'zabytki-nieruchome' => 'rejestr zabytków',
            'stanowiska-archeologiczne' => 'rejestr zabytków'
        );
        $protection_type_for_name = array(
            'unesco' => 'Światowe dziedzictwo UNESCO',
            'pomniki-historii' => 'Pomnik historii',
            'zabytki-nieruchome' => 'Zabytek nieruchomy',
            'stanowiska-archeologiczne' => 'Stanowisko archeologiczne'
        );
        foreach ($response as $monument) {
            if (isset($monument->inspire_id)) {
                $count++;
                $name = $monument->name ? $monument->name : ($monument->protection_type ? (isset($protection_type_for_name[$monument->protection_type]) ? $protection_type_for_name[$monument->protection_type] : str_replace('-', ' ', $monument->protection_type)) : __('<i>Zabytek (brak nazwy)</i>', 'nid'));
                $address = $monument->address_city . ($monument->address_street ? ', ' . $monument->address_street . ' ' . $monument->address_number : '');
                $html .= '<li><div class="relic-list__img-wrap">';
                if ($monument->cover)
                    $html .= '<div class="relic-list__img" style="background-image: url(' . $monument->cover->thumbnail . ')"></div>';
                else
                    $html .= '<div class="relic-list__img no-image" style="background-color: inherit; background-size: contain; background-image: url(' . get_theme_file_uri('/assets/images/tarcza_nt.png') . ')"></div>';
                $html .= '</div><div class="relic-list__desc-wrap">';
                if (isset($monument->zabytek_url) && $monument->zabytek_url)
                    $html .= '<a href="' . $monument->zabytek_url . '" target="_blank" class="relic-list__title-link"><div class="relic-list__title">' . $name . '</div></a>';
                else
                    $html .= '<div class="relic-list__title relic-list__title-nolink">' . $name . '</div>';
                $html .= '<div class="relic-list__desc"><span class="relic-list__label">Rodzaj:</span> ' . (isset($collecion[$monument->collection]) ? $collecion[$monument->collection] : str_replace('-', ' ', $monument->collection)) . '</div>';
                $html .= '<div class="relic-list__desc"><span class="relic-list__label">Chronologia:</span> ' . $monument->year_txt . '</div>';
                $html .= '<div class="relic-list__desc"><span class="relic-list__label">Forma ochrony:</span> ' . (isset($protection_type[$monument->protection_type]) ? $protection_type[$monument->protection_type] : str_replace('-', ' ', $monument->protection_type)) . '</div>';
                $html .= '<div class="relic-list__desc"><span class="relic-list__label">Adres:</span> ' . $address . '</div>';
                if (isset($monument->zabytek_url) && $monument->zabytek_url)
                    $html .= '<div class="relic-list__desc"><a class="relic-list__link" href="' . $monument->zabytek_url . '" target="_blank">Więcej</a></div>';
                $html .= '</div></li>';
            }
        };
        if (!$count)
            $html = '<div class="relic-list__no-results">Nie znaleziono więcej zabytków.</div>';
        $params['count'] = true;
//        if ($pages = json_decode(file_get_contents($zabytek_api_url . $zabytek_api_ver . http_build_query($params)))) {
        if ($pages = json_decode(file_get_contents($zabytek_api_url . $zabytek_api_ver . http_build_query($params), false, stream_context_create($arrContextOptions)))) {
            $pagination = array(
                'count' => $pages->count,
                'page' => isset($_POST['page']) ? $_POST['page'] : 1,
                'paths_per_page' => $params['page_limit']
            );
            ob_start();
            include(locate_template('partials/nid_pagination_monuments.php'));
            $pages_html = ob_get_clean();
        }
        $post_data = array(
            'status' => true,
            'html' => $html,
            'next' => $count == $params['page_limit'],
            'pagination' => $pages_html,
            'results_count' => isset($pagination['count']) ? $pagination['count'] : false
        );
    } else
        $post_data = array('status' => false, 'error' => 'Błąd połączenia z <a href="' . $zabytek_api_url . '" target="_blank">bazą zabytków</a>. Przepraszamy.');
    echo json_encode($post_data);
    wp_die();
}

add_action('wp_ajax_nid_monuments_search', 'nid_monuments_search');
add_action('wp_ajax_nopriv_nid_monuments_search', 'nid_monuments_search');
