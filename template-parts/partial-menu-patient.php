<?php
/**
 * ===============================
 * PARTIAL MENU PATIENT .PHP - display bottom menu on patient site
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$footer_patient_section = get_option('options_footer_patient_section');
$footer_patient_img = get_field( 'footer_patient_img', 'option' );
?>
<div class="menu__bottom">
	<div class="container">
		<div class="menu__bottom-cnt">
			<div class="menu__bottom-left">
				<h3><?php esc_html_e( $footer_patient_section, 'cbk' ); ?></h3>
				<?php if ( have_rows( 'footer_patient', 'option' ) ) : ?>
					<ul>
						<?php while ( have_rows( 'footer_patient', 'option' ) ) : the_row(); ?>
							<?php $footer_patient_link = get_sub_field( 'footer_patient_link' );?>
							<li>
								<a href="<?php echo esc_url( $footer_patient_link); ?>"><?php the_sub_field( 'footer_patient_name' ); ?></a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="menu__bottom-right">
				<img src="<?php echo $footer_patient_img;?>" alt="">
			</div>
		</div>
	</div>	
</div>



