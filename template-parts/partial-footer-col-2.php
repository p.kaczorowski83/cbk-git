<?php
/**
 * ===============================
 * FOOTER COL 2 .PHP - footer col 2
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="footer__col2">
    
    <?php 
    get_template_part( 'template-parts/partial', 'footer-about' );
    get_template_part( 'template-parts/partial', 'footer-patient' );
    get_template_part( 'template-parts/partial', 'footer-doctor' );
    get_template_part( 'template-parts/partial', 'footer-sponsor' );
    ?> 

</div>