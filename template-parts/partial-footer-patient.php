<?php
/**
 * ===============================
 * FOOTER PATIENT .PHP - footer patient coll
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_patient_section = get_option('options_footer_patient_section');
?>
<?php if ( have_rows( 'footer_patient', 'option' ) ) : ?>
<div class="footer__col">
	<p><?php esc_html_e( $footer_patient_section, 'cbk' ); ?></p>	
	<ul>
		<?php while ( have_rows( 'footer_patient', 'option' ) ) : the_row(); ?>
			<?php $footer_patient_link = get_sub_field( 'footer_patient_link' );?>
			<li>
				<a href="<?php echo esc_url( $footer_patient_link); ?>"><?php the_sub_field( 'footer_patient_name' ); ?></a>
			</li>
		<?php endwhile; ?>
	</ul>
</div>
<?php endif; ?>
