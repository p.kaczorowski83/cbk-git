<?php
/**
 * ===============================
 * PARTIAL AOUT BOXES .PHP - display boxes 50/50 in about page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$about_us_why_title = get_post_meta(get_the_ID(), 'about_us_why_title', true );

$allowed_types = array(
	'br'     => array(),
	'p'     => array(),
	'strong'     => array(),
	'li'     => array(),
	'ul'     => array(),
);
?>

<?php if ( have_rows( 'about_boxes' ) ) : ?>
<!-- BOXES -->
<ul class="boxes__50-50 container">
		<?php $i=0; while ( have_rows( 'about_boxes' ) ) : the_row();
		$about_boxes_title = get_sub_field('about_boxes_title');
		$about_boxes_txt = get_sub_field('about_boxes_txt');
		$i++
		?>
		<li>
			<!-- TXT -->
			<div class="boxes__50-50-txt <?php if ($i % 2 != 0): ?>order_2<?php endif;?>">
				<div class="d-flex align-items-center h-100">
					<div class="row">	
						<h3 class="typo2"><?php esc_html_e( $about_boxes_title , 'cbk' ); ?></h3>
						<?php echo wp_kses( __( $about_boxes_txt, 'cbk' ), $allowed_types ); ?>
						<?php $about_boxes_link = get_sub_field( 'about_boxes_link' ); ?>
						<?php if ( $about_boxes_link ) : ?>
							<a href="<?php echo esc_url( $about_boxes_link['url'] ); ?>" class="btn__base"><?php echo esc_html( $about_boxes_link['title'] ); ?></a>
						<?php endif; ?>
					</div>	
				</div>
			</div>	

			<!-- FOTO -->
			<div class="boxes__50-50-foto <?php if ($i % 2 != 0): ?>order_1<?php endif;?>">
				<?php $about_boxes_img = get_sub_field( 'about_boxes_img' ); ?>
				<?php if ( $about_boxes_img ) : ?>
					<img loading="lazy" class="lazyload" data-src="<?php echo esc_url( $about_boxes_img['sizes']['image610'] ); ?>" alt="<?php echo esc_attr( $about_boxes_img['alt'] ); ?>" />
				<?php endif; ?>
			</div>	
		</li>
	<?php endwhile; ?>
</ul>
<?php endif?>



