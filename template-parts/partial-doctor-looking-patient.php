<?php
/**
 * ===============================
 * PARTIAL OOKING PATIENT.PHP - display looking patient section on medical examination page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$looking_patient_title = get_post_meta(get_the_ID(), 'looking_patient_title', true );
$looking_patient_cnt = get_field('looking_patient_cnt');
$looking_patient_info = get_post_meta(get_the_ID(), 'looking_patient_info', true );

$allowed_types = array(
	'br'     => array(),
	'p'     => array(),
	'strong'     => array(),
	'li'     => array(),
	'ul'     => array(),
	'a'=> array(
		'href' => array()),

);
?>

<div class="doctor__looking">
	<h3 class="typo2"><?php esc_html_e($looking_patient_title, 'cbk'); ?></h3>
	<?php echo $looking_patient_cnt ?>

	<div class="doctor__looking-info">
		<?php echo wp_kses( __( $looking_patient_info, 'cbk' ), $allowed_types ); ?>
	</div>

	<?php $looking_patient_link = get_field( 'looking_patient_link' ); ?>
	<?php if ( $looking_patient_link ) : ?>
		<a href="<?php echo esc_url( $looking_patient_link['url'] ); ?>" class="btn__orange" target="<?php echo esc_attr( $looking_patient_link['target'] ); ?>"><?php echo esc_html( $looking_patient_link['title'] ); ?></a>
	<?php endif; ?>

</div>



