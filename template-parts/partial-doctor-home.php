<?php
/**
 * ===============================
 * PARTIAL DOCTOR HOME .PHP - display doctor home page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$doctor_title = get_post_meta(get_the_ID(), 'doctor_title', true );
$doctor_cnt = get_post_meta(get_the_ID(), 'doctor_cnt', true );
$image = get_post_meta( get_the_ID(), 'doctor_img', true );

$allowed_types = array(
	'br'     => array(),
	'p'     => array(),
	'strong'     => array(),
);

?>


<div class="doctor__home">
	<div class="container">
		<div class="doctor__home-breadcrumb">
			<!-- breadcrumb -->
			<?php echo bcn_display();?>
		</div>
		<ul>	
			<li>
				<h1 class="typo2"><?php esc_html_e($doctor_title, 'cbk'); ?></h1>	
				<p><?php echo wp_kses( __( $doctor_cnt, 'cbk' ), $allowed_types ); ?></p>

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>dla-lekarzy/aktualnie-prowadzone-badania/" title="Tak, potwierdzam" class="ok">TAK, POTWIERDZAM</a>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="back" title="Nie, opuszczam stronę">NIE, OPUSZAM STRONĘ</a>
			</li>
			<li>	
				<?php echo  wp_get_attachment_image( $image,'') ?>
			</li>
		</ul>
	</div>	
</div>



