<?php
/**
 * ===============================
 * FOOTER COL 1  .PHP - footer col 1
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$footer_address = get_option('options_footer_address');

$allowed_types = array(
    'br'     => array(),
    'strong'     => array(),
    'a'     => array(
    	'href'     => array()
    ),
);
?>

<div class="footer__col1">
    <div class="row">  
        <img loading="lazy" class="lazyload" data-src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo-footer.svg" alt="<?php bloginfo('name'); ?>">
    </div> 
    <?php echo wp_kses( __( $footer_address, 'cbk' ), $allowed_types ); ?>
</div>