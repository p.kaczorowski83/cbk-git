<?php
/**
 * ===============================
 * PARTIAL GALLERY .PHP - display gallery section - title, content, foto
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="gallery">
	<div class="container">
		<?php if ( have_rows( 'sponsor_gallery' ) ) : ?>
			<?php $z=0;while ( have_rows( 'sponsor_gallery' ) ) : the_row(); ?>
				<h3 class="typo2a text-center"><?php the_sub_field( 'sponsor_gallery_title' ); ?></h3>
				<p class="text-center"><?php the_sub_field( 'sponsor_gallery_cnt' ); ?></p>
				<?php $z++; $sponsor_gallery_foto_images = get_sub_field( 'sponsor_gallery_foto' ); ?>
				<?php if ( $sponsor_gallery_foto_images ) :  ?>
					<ul>
					<?php $i=0; foreach ( $sponsor_gallery_foto_images as $sponsor_gallery_foto_image ): ?>
					<?php $i++;?>
						<li>
							<a href="<?php echo esc_url( $sponsor_gallery_foto_image['url'] ); ?>" data-fancybox="gal-<?php echo $z;?>" data-caption="<?php echo esc_attr( $sponsor_gallery_foto_image['alt'] ); ?>">
								<img src="<?php echo esc_url( $sponsor_gallery_foto_image['sizes']['image295'] ); ?>" alt="<?php echo esc_attr( $sponsor_gallery_foto_image['alt'] ); ?>" />
							</a>

						</li>
						<?php if($i==4) break; ?>	
					<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			<?php endwhile; ?>
			</ul>
	<?php endif; ?>
	</div>	
</div>



