<?php
/**
 * ===============================
 * PARTIAL MENU SPONSOR .PHP - display bottom menu on for sponsor site
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$footer_sponsor_section = get_option('options_footer_sponsor_section');
$footer_sponsor_img = get_field( 'footer_sponsor_img', 'option' );
?>
<div class="menu__bottom">
	<div class="container">
		<div class="menu__bottom-cnt">
			<div class="menu__bottom-left">
				<h3><?php esc_html_e( $footer_sponsor_section, 'cbk' ); ?></h3>
				<?php if ( have_rows( 'footer_sponsor', 'option' ) ) : ?>
					<ul>
						<?php while ( have_rows( 'footer_sponsor', 'option' ) ) : the_row(); ?>
							<?php $footer_sponsor_link = get_sub_field( 'footer_sponsor_link' );?>
							<li>
								<a href="<?php echo esc_url( $footer_sponsor_link); ?>"><?php the_sub_field( 'footer_sponsor_name' ); ?></a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="menu__bottom-right">
				<img src="<?php echo $footer_sponsor_img;?>" alt="">
			</div>
		</div>
	</div>	
</div>



