<?php
/**
 * ===============================
 * PARTIAL DOCTOR HOME .PHP - display doctor home page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */


?>


<div class="doctor__research">
	<div class="container">
		<ul>
			<?php
	        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	        $args = array(
	        'post_type' => array('aktualne-badania'),
	        'posts_per_page' => 10,
	        'post_status' => array('publish'),
	        'paged' => $paged
	    	);

		    $loop = new WP_Query( $args );
		    if ( $loop->have_posts() ) {
		    while ( $loop->have_posts() ) : $loop->the_post();

		    $content_research = get_post_meta(get_the_ID(), 'content_research', true );
		    ?>
				<li>
					<h2>
						<?php echo the_title(); ?>
					</h2>
					<p><?php esc_html_e($content_research, 'cbk'); ?></p>
				</li>
			<?php endwhile; ?>
			</ul>
		<?php } ?>
	</div>	
</div>



