<?php
/**
 * ===============================
 * PARTIAL PATIENT FORM .PHP - display form section on medical examination page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$patient_form_title = get_post_meta(get_the_ID(), 'patient_form_title', true );
$patient_form_lead = get_post_meta(get_the_ID(), 'patient_form_lead', true );
$patient_form_phone = get_post_meta(get_the_ID(), 'patient_form_phone', true );
$patient_form_email = get_post_meta(get_the_ID(), 'patient_form_email', true );

$formularz_shortocde = get_field('formularz_shortocde');


?>


<div class="patient__form">
	<div class="container">
		<h2 class="typo2a text-center"><?php esc_html_e($patient_form_title, 'cbk'); ?></h2>		
		<p class="text-center"><?php esc_html_e($patient_form_lead, 'cbk' ); ?></p>
		<ul>
			<li>
				<a href="tel:<?php esc_html_e($patient_form_phone, 'cbk'); ?>">
					<?php esc_html_e($patient_form_phone, 'cbk'); ?>
				</a>
			</li>
			<li>
				<a href="mailto:<?php esc_html_e($patient_form_email, 'cbk'); ?>">
					<?php esc_html_e($patient_form_email, 'cbk'); ?>
				</a>
			</li>
		</ul>
		<?php echo $formularz_shortocde;?>
	</div>	
</div>



