<?php
/**
 * ===============================
 * FOOTER ABOUT .PHP - footer about coll
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_about_section = get_option('options_footer_about_section');
?>

<div class="footer__col">
	<p><?php esc_html_e( $footer_about_section, 'cbk' ); ?></p>	
	<?php if ( have_rows( 'footer_about', 'option' ) ) : ?>
		<ul>
			<?php while ( have_rows( 'footer_about', 'option' ) ) : the_row(); ?>
				<?php $footer_about_link = get_sub_field( 'footer_about_link' );?>
				<li>
					<a href="<?php echo esc_url( $footer_about_link); ?>"><?php the_sub_field( 'footer_about_name' ); ?></a>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
</div>
