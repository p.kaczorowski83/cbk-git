<?php
/**
 * ===============================
 * PARTIAL INFO BOX .PHP - display info section on bottom page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$info_haslo = get_post_meta(get_the_ID(), 'info_haslo', true );
$info_txt = get_post_meta(get_the_ID(), 'info_txt', true );
$info_link = get_post_meta(get_the_ID(), 'info_link', true );

$allowed_types = array(
	'br'     => array(),
);

?>

<?php if ( $info_haslo ) :  ?>
<div class="info__box">
	<div class="container">	

		<h2><?php _e( $info_haslo , 'cbk' ); ?></h2>

		<?php if ($info_txt) :?>
			<p>	<?php echo wp_kses( __( $info_txt, 'cbk' ), $allowed_types ); ?></p>
		<?php endif;?>

		<?php if ( $info_link ) : ?>
			<div class="text-center">	
				<a href="<?php echo esc_url( $info_link['url'] ); ?>" class="btn__orange"><?php echo esc_html( $info_link['title'] ); ?></a>
			</div>
		<?php endif; ?>

		
	</div>	
</div>
<?php endif;?>



