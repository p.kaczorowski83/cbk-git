<?php
/**
 * ===============================
 * PARTIAL MENU PATIENT .PHP - display bottom menu on for doctor site
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$footer_doctor_section = get_option('options_footer_doctor_section');
$footer_doctor_img = get_field( 'footer_doctor_img', 'option' );
?>
<div class="menu__bottom">
	<div class="container">
		<div class="menu__bottom-cnt">
			<div class="menu__bottom-left">
				<h3><?php esc_html_e( $footer_doctor_section, 'cbk' ); ?></h3>
				<?php if ( have_rows( 'footer_doctor', 'option' ) ) : ?>
					<ul>
						<?php while ( have_rows( 'footer_doctor', 'option' ) ) : the_row(); ?>
							<?php $footer_doctor_link = get_sub_field( 'footer_doctor_link' );?>
							<li>
								<a href="<?php echo esc_url( $footer_doctor_link); ?>"><?php the_sub_field( 'footer_doctor_name' ); ?></a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="menu__bottom-right">
				<img src="<?php echo $footer_doctor_img;?>" alt="">
			</div>
		</div>
	</div>	
</div>



