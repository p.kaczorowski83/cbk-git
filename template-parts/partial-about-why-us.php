<?php
/**
 * ===============================
 * PARTIAL AOUT WHY US .PHP - display why us in about page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$about_us_why_title = get_post_meta(get_the_ID(), 'about_us_why_title', true );

$allowed_types = array(
	'br'     => array(),
);
?>

<div class="about__why">
	<div class="container">	
		<h2><?php esc_html_e( $about_us_why_title , 'cbk' ); ?></h2>

		<?php if ( have_rows( 'about_us_why' ) ) : ?>
			<ul>
				<?php while ( have_rows( 'about_us_why' ) ) : the_row(); 
				$about_us_why_txt = get_sub_field('about_us_why_txt');
				?>
					<li>
						<?php $about_us_why_icon = get_sub_field( 'about_us_why_icon' ); ?>
						<?php if ( $about_us_why_icon ) : ?>
							<img loading="lazy" class="lazyload" data-src="<?php echo esc_url( $about_us_why_icon['url'] ); ?>" alt="<?php echo esc_attr( $about_us_why_icon['alt'] ); ?>" />
						<?php endif; ?>
						<p><?php echo wp_kses( __( $about_us_why_txt, 'cbk' ), $allowed_types ); ?></p>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif;?>
	</div>
</div>


