<?php
/**
 * ===============================
 * FOOTER SPONSOR .PHP - footer sponsor coll
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_sponsor_section = get_option('options_footer_sponsor_section');
?>

<div class="footer__col">
	<p><?php esc_html_e( $footer_sponsor_section, 'cbk' ); ?></p>	
	<?php if ( have_rows( 'footer_sponsor', 'option' ) ) : ?>
		<ul>
			<?php while ( have_rows( 'footer_sponsor', 'option' ) ) : the_row(); ?>
				<?php $footer_sponsor_link = get_sub_field( 'footer_sponsor_link' ); ?>
				<li>
					<a href="<?php echo esc_url( $footer_sponsor_link); ?>"><?php the_sub_field( 'footer_sponsor_name' ); ?></a>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
</div>
