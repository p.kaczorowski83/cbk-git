<?php
/**
 * ===============================
 * HEADER-LOGO.PHP - logo
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo-CBK.svg" alt="<?php bloginfo('name'); ?>" class="img-fluid">