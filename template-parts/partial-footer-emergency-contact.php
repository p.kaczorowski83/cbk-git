<?php
/**
 * ===============================
 * FOOTER EMERGENCY CONTACT .PHP - footer emergency contact section
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_emergency = get_option('options_footer_emergency');
$footer_emergency_tel = get_option('options_footer_emergency_tel');
$phone = str_replace(' ', '', get_option("options_footer_emergency_tel"));

$allowed_types = array(
    'span'     => array(),
);

?>

<div class="footer__emergency">
    <div class="container"> 
        <div class="row">
            <div class="col col-lg-7 col-md-7 col-sm-12 col-12">
                <div class="footer__emergency-txt">
                   <?php echo wp_kses( __( $footer_emergency, 'cbk' ), $allowed_types ); ?>
                </div>
            </div>
            <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                <div class="footer__emergency-tel"> 
                    <a href="tel:<?php echo $phone ?>"><?php esc_html_e( $footer_emergency_tel, 'cbk' ); ?></a>
                </div>
            </div>
        </div>
    </div>  
</div>

