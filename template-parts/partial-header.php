<?php
/**
 * ===============================
 * HEADER.PHP - main header file
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
?>


<header class="navbar__fixed">	
	
	<!-- LOGO -->
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar__fixed-logo">
		<?php 
		get_template_part( 'template-parts/partial', 'header-logo' ); 
		?>	
	</a>
	
	<!-- MENU  -->
	<nav>
		<?php
		wp_nav_menu(
			array(
			'theme_location' => 'main-menu',
			'container'      => '',
			'menu_class'     => 'navbar__fixed-nav',
			'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
			'walker'         => new WP_Bootstrap_Navwalker(),
			)
		);
		?>		
	</nav> <!-- end nav -->

	<!-- WCAG and LANG -->
	<div class="navbar__fixed-wcag-lang">
		<?php echo do_shortcode('[ic_wcag]');?>
		<?php echo do_action('wpml_add_language_selector');?>
	</div>



	<!-- HAMBURGER -->
	<div class="hamburger">
		<div class="line line1"></div>
		<div class="line line2"></div>
		<div class="line line3"></div>
	</div>
	
</header>