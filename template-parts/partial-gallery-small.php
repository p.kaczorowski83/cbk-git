<?php
/**
 * ===============================
 * PARTIAL GALLERY SMALL .PHP - display gallery section - title, content, foto
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="gallery">
	<div class="container">
		<?php if ( have_rows( 'sponsor_gallery' ) ) : ?>
			<div class="gallery__container">
				<?php $ii=0; while ( have_rows( 'sponsor_gallery' ) ) : the_row();
				$ii++; ?>
				<div class="gallery__coll" id="gallery-<?php echo $ii;?>">
					<h3 class="typo2"><?php the_sub_field( 'sponsor_gallery_title' ); ?></h3>
					<p><?php the_sub_field( 'sponsor_gallery_cnt' ); ?></p>
					<?php $sponsor_gallery_foto_images = get_sub_field( 'sponsor_gallery_foto' ); ?>
					<?php if ( $sponsor_gallery_foto_images ) :  ?>
						<ul>
						<?php $i=0; foreach ( $sponsor_gallery_foto_images as $sponsor_gallery_foto_image ): ?>
							<li>
								<a href="<?php echo esc_url( $sponsor_gallery_foto_image['url'] ); ?>" data-fancybox="gallery-<?php echo $ii;?>" id="btn-<?php echo $ii;?>" data-caption="<?php echo esc_attr( $sponsor_gallery_foto_image['alt'] ); ?>">
									<img src="<?php echo esc_url( $sponsor_gallery_foto_image['sizes']['image610'] ); ?>" alt="<?php echo esc_attr( $sponsor_gallery_foto_image['alt'] ); ?>" class="img-fluid" />
								</a>
							</li>							
						<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>	
				<?php endwhile; ?>
			</div>	
	<?php endif; ?>
	</div>	
</div>



