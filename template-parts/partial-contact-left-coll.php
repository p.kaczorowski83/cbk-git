<?php
/**
 * ===============================
 * CONTACT LEFT COLL.PHP - contact left coll
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

?>


<div class="contact__left">

	<?php if ( have_rows( 'contact_left' ) ) : ?>
		<?php while ( have_rows( 'contact_left' ) ) : the_row(); ?>
			<h4 class="typo2"><?php the_sub_field( 'contact_left_name' ); ?></h4>
			<?php the_sub_field( 'contact_left_cnt' ); ?>
		<?php endwhile; ?>
	<?php endif; ?>
    
</div>
