<?php
/**
 * ===============================
 * HEADER HERO.PHP - hero
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$hero_title = get_post_meta(get_the_ID(), 'hero_title', true );
$hero_txt = get_field('hero_txt');

$allowed_types = array(
	'br'     => array(),
	'strong' => array(),
	'p'      => array(),
);

?>

<?php if ($hero_title) :?>
<div class="hero">
	<div class="container">	
		<div class="hero__breadcrumb">
			<!-- breadcrumb -->
			<?php echo bcn_display();?>
		</div>
		<h1 class="typo2"><?php esc_html_e( $hero_title , 'cbk' ); ?></h1>
		<?php echo wp_kses( __( $hero_txt, 'cbk' ), $allowed_types ); ?>
	</div>
</div>
<?php endif;?>