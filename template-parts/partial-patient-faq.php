<?php
/**
 * ===============================
 * PARTIAL PATIENT FAQ .PHP - display faq section
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$boxes_title = get_post_meta(get_the_ID(), 'boxes_title', true );

$allowed_types = array(
	'br'     => array(),
	'p'     => array(),
	'strong'     => array(),
	'li'     => array(),
	'ul'     => array(),
);

?>


<div class="patient">
	<div class="container">
		<?php if ( have_rows( 'faq' ) ) : ?>
		<ul class="patient__faq">
		<?php while ( have_rows( 'faq' ) ) : the_row(); ?>
			<li>
				<p class="q"><?php the_sub_field( 'faq_question' ); ?></p>
				<div class="a"><?php the_sub_field( 'faq_reply' ); ?></div>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php endif;?>
	</div>	
</div>



