<?php
/**
 * ===============================
 * FOOTER COL ASSOCIATION .PHP - footer association
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_association = get_option('options_footer_association');

$allowed_types = array(
    'strong'     => array(),
);

?>
<p><?php echo wp_kses( __( $footer_association, 'cbk' ), $allowed_types ); ?></p>

<?php if ( have_rows( 'footer_association_loga', 'option' ) ) : ?>
	<ul>
	<?php while ( have_rows( 'footer_association_loga', 'option' ) ) : the_row(); ?>
		<?php $footer_association_loga_img = get_sub_field( 'footer_association_loga_img' ); ?>
		<?php if ( $footer_association_loga_img ) : ?>
			<li>
				<img loading="lazy" class="lazyload" data-src="<?php echo esc_url( $footer_association_loga_img['url'] ); ?>" alt="<?php echo esc_attr( $footer_association_loga_img['alt'] ); ?>" />
			</li>
		<?php endif; ?>
	<?php endwhile; ?>
	</ul>
<?php endif; ?>