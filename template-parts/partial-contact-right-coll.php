<?php
/**
 * ===============================
 * CONTACT RIGHT COLL.PHP - contact right coll
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$contact_right_location_title = get_post_meta(get_the_ID(), 'contact_right_location_title', true );
$contact_right_forms = get_post_meta(get_the_ID(), 'contact_right_forms', true );

$maps = get_post_meta( get_the_ID(), 'contact_right_location_maps', true );

?>

<div class="contact__right">
    <h4 class="typo2"><?php esc_html_e( $contact_right_location_title , 'cbk' ); ?></h4>

    <!-- MAPA -->
    <div class="contact__right-maps">
       <?php echo $maps ?>
    </div>

    <!-- FORMULARZ -->
    <div class="contact__right-form">
        <h4 class="typo2"><?php esc_html_e( $contact_right_forms , 'cbk' ); ?></h4>
        <?php if(ICL_LANGUAGE_CODE == 'en') :?>
            <?php echo do_shortcode('[contact-form-7 id="1448" title="Contact-en"]');?>
        <?php else :?>
            <?php echo do_shortcode('[contact-form-7 id="258" title="Kontakt"]');?>
        <?php endif;?>
    </div>
</div>


