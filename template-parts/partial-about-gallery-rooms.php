<?php
/**
 * ===============================
 * PARTIAL ABOUT GALLERY ROOMS .PHP - display gallery rooms on about page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$about_gallery_title = get_post_meta(get_the_ID(), 'about_gallery_title', true );
$about_gallery_link = get_post_meta(get_the_ID(), 'about_gallery_link', true );
?>

<?php $about_gallery_images = get_field( 'about_gallery' ); ?>
<?php if ( $about_gallery_images ) :  ?>
<div class="about__gallery">
	<div class="container">	

		<h2><?php esc_html_e( $about_gallery_title , 'cbk' ); ?></h2>

		<div class="about__gallery-grid">
			<?php $i=0; foreach ( $about_gallery_images as $about_gallery_image ): ?>
				<?php $i++?>
				<figure class="about__gallery-item--<?php echo $i;?>">
					<img loading="lazy" class="lazyload" data-src="<?php echo esc_url( $about_gallery_image['sizes']['image610'] ); ?>" alt="<?php echo esc_attr( $about_gallery_image['alt'] ); ?>" />
				</figure>
			<?php endforeach; ?>
		</div>

		<?php if ( $about_gallery_link ) : ?>
			<div class="text-center">
				<a href="<?php echo esc_url( $about_gallery_link['url'] ); ?>" class="btn__base"><?php echo esc_html( $about_gallery_link['title'] ); ?></a>
			</div>
		<?php endif; ?>

	</div>	
</div>
<?php endif;?>



