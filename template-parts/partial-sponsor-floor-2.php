<?php
/**
 * ===============================
 * PARTIAL SPONSOR-FLOOR 2 .PHP - display floor 2
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$room__title = get_post_meta(get_the_ID(), 'room__title', true );
$room__cnt = get_post_meta(get_the_ID(), 'room__cnt', true );

$allowed_types = array(
	'br'     => array(),
	'strong' => array(),
	'p'      => array(),
);

?>

<div class="sponsor__floor">
	<div class="container">
		<h2 class="typo2a text-center"><?php esc_html_e($room__title, 'cbk'); ?></h2>

		<?php
			get_template_part( 'template-parts/partial', 'sponsor-menu-floor' );
		?>
		
		<p class="sponsor__floor-lead"><?php echo wp_kses( __( $room__cnt, 'cbk' ), $allowed_types ); ?></p>

		<div class="sponsor__floor-plan">
			<?php
			get_template_part( 'template-parts/partial', 'sponsor-plan-2' );
			?>
		</div>
	</div>
</div>