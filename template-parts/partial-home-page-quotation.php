<?php
/**
 * ===============================
 * HOME PAGE QUOTATION.PHP - home page quotation section
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$home_quotation_title = get_post_meta(get_the_ID(), 'home_quotation_title', true );
$home_quotation_cnt = get_post_meta(get_the_ID(), 'home_quotation_cnt', true );
$home_quotation_author = get_post_meta(get_the_ID(), 'home_quotation_author', true );
?>

<section class="home__quotation">
    <div class="home__quotation-box">
        <div class="home__quotation-txt">
            <?php if ($home_quotation_title) :?>
            <h4><?php echo $home_quotation_title;?></h4>
            <?php endif;?>
            
            <?php if ($home_quotation_cnt):?>
            <p><?php echo $home_quotation_cnt;?></p>
            <?php endif;?>
            
            <?php if ($home_quotation_author):?>
            <span><?php echo $home_quotation_author;?></span>
            <?php endif;?>
        </div>  
    </div>    
</section>

