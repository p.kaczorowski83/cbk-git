<?php
/**
 * ===============================
 * HOME PAGE SELECT GROUP.PHP - home page select group section
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$home_select_title = get_post_meta(get_the_ID(), 'home_select_title', true );

$home_select_sizes = '(max-width:600px) 600px, (max-width: 1920px) 500px, 700px';

?>

<section class="home__select">
	<?php if ($home_select_title) :?>
	<div class="container">	
		<h2><?php echo $home_select_title;?></h2>	
	</div>	
	<?php endif;?>
	<?php if ( have_rows( 'home_select' ) ) : ?>
	<ul class="list__box">
		<?php $i=0; while ( have_rows( 'home_select' ) ) : the_row(); ?>
			<li>
				<div class="list__box-item">	
					<!-- TXT -->
					<div class="list__box-txt <?php if ($i % 2 != 0): ?>order_2<?php endif;?>">
						<h3 class="typo2"><?php the_sub_field( 'home_select_title' ); ?></h3>
						<p><?php the_sub_field( 'home_select_cnt' ); ?></p>
						<?php $home_select_link = get_sub_field( 'home_select_link' ); ?>
						<?php if ( $home_select_link ) : ?>
							<a href="<?php echo esc_url( $home_select_link['url'] ); ?>" class="btn__base"><?php echo esc_html( $home_select_link['title'] ); ?></a>
						<?php endif; ?>
					</div>	

					<!-- FOTO -->
					<div class="list__box-foto <?php if ($i % 2 != 0): ?>order_1<?php endif;?>">
						<?php $home_select_img = get_sub_field( 'home_select_img' ); ?>
						<?php if ( $home_select_img ) : ?>
							<img loading="lazy" class="lazyload" data-src="<?php echo $home_select_img['sizes']['image700'];?>" alt="">
						<?php endif; ?>
					</div>	
				</div>		
			</li>
		<?php endwhile; ?>		
	</ul>
	<?php endif; ?>

</section>

