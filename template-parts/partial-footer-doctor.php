<?php
/**
 * ===============================
 * FOOTER DOCTOR .PHP - footer doctor coll
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_doctor_section = get_option('options_footer_doctor_section');
?>

<?php if ( have_rows( 'footer_doctor', 'option' ) ) : ?>
<div class="footer__col">
	<p><?php esc_html_e( $footer_doctor_section, 'cbk' ); ?></p>	
	<ul>
		<?php while ( have_rows( 'footer_doctor', 'option' ) ) : the_row(); ?>
			<?php $footer_doctor_link = get_sub_field( 'footer_doctor_link' ); ?>
			<li>
				<a href="<?php echo esc_url( $footer_doctor_link); ?>"><?php the_sub_field( 'footer_doctor_name' ); ?></a>
			</li>
		<?php endwhile; ?>
	</ul>
</div>
<?php endif; ?>