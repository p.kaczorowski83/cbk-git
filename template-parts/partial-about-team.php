<?php
/**
 * ===============================
 * PARTIAL AOUT TEAM .PHP - display list person on team page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$team_info = get_post_meta(get_the_ID(), 'team_info', true );
?>

<div class="about__team">
	<div class="container">	

		<?php if ( have_rows( 'team' ) ) : ?>
			<?php while ( have_rows( 'team' ) ) : the_row(); ?>

				<?php if ( have_rows( 'team_row' ) ) : ?>
					<ul>
					<?php while ( have_rows( 'team_row' ) ) : the_row(); ?>
						<?php $team_title = get_sub_field('team_title');
						$team_name = get_sub_field('team_name');
						$team_position = get_sub_field('team_position');
						?>
						<li>
							<?php $team_img = get_sub_field( 'team_img' ); ?>
							<?php if ( $team_img ) : ?>
								<img loading="lazy" class="lazyload img-fluid" data-src="<?php echo esc_url( $team_img['sizes']['image388'] ); ?>" alt="<?php echo esc_attr( $team_img['alt'] ); ?>" />
							<?php else :?>
								<img loading="lazy" class="lazyload img-fluid" data-src="<?php echo get_template_directory_uri(); ?>/assets/svg/icon-avatar.svg" alt="" />
							<?php endif; ?>
							<?php if ($team_title) :?><span><?php the_sub_field( 'team_title' ); ?></span><?php endif;?>
							<?php if ($team_name) :?><h3><?php the_sub_field( 'team_name' ); ?></h3><?php endif;?>
							<?php if ($team_position):?><p><?php the_sub_field( 'team_position' ); ?></p><?php endif;?>
						</li>	
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>

		<?php if ($team_info):?>
		<!-- INFO -->
		<div class="about__team-info">
			<p><?php _e( $team_info , 'cbk' ); ?></p>
		</div>
		<?php endif;?>

	</div><!-- end .container -->
</div>	
