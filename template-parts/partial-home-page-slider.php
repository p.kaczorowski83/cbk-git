<?php
/**
 * ===============================
 * HOME PAGE SLIDER.PHP - home page slider section
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$home_offer_title = get_post_meta(get_the_ID(), 'home_offer_title', true )
?>

<div class="slider">
    <ul class="slider_list">
        <?php $i=0; while ( have_rows( 'slider' ) ) : the_row(); ?>
		<li data-number="<?php echo ++$i;?>">
            <div class="slider__item">
                <?php 
                $slider_img = get_sub_field( 'slider_img' );
                $slider_link = get_sub_field( 'slider_link' );
                ?>
                <img src="<?php echo $slider_img['sizes']['image1920'];?>" alt="">
                <div class="slider__cnt">
                    <p><?php the_sub_field( 'slider_title' ); ?></p>
                    <span><?php the_sub_field( 'slider_cnt' ); ?></span>
                    <?php if ( $slider_link ) : ?>
                        <a href="<?php echo esc_url( $slider_link['url'] ); ?>"><?php echo esc_html_e( $slider_link['title'],'cbk' ); ?></a>
                    <?php endif; ?>
                </div>
            </div> 
        </li>
	    <?php endwhile; ?>
    </ul>
</div>

