<?php
/**
 * ===============================
 * PARTIAL SPONSOR DOWNLOAD .PHP - display download file list
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$research_cnt = get_field('research_cnt');

?>

<?php if ( have_rows( 'download' ) ) : ?>		
<div class="download">	
	<div class="container">	
		<?php while ( have_rows( 'download' ) ) : the_row(); ?>
		<h3 class="typo2"><?php the_sub_field( 'download_title' ); ?></h3>
		<?php if ( have_rows( 'download_file' ) ) : ?>
			<ul>	
			<?php while ( have_rows( 'download_file' ) ) : the_row(); ?>
				<?php $download_file_files = get_sub_field( 'download_file_files' ); ?>
				<?php if ( $download_file_files ) : ?>
					<li>
						<a href="<?php echo esc_url( $download_file_files['url'] ); ?>" download><?php the_sub_field( 'download_file_name' ); ?></a>
						    <div class="download__icon">
								<a href="<?php echo esc_url( $download_file_files['url'] ); ?>" download>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/icon-download.svg" alt="">	
								</a>
						    </div>	
					</li>	
				<?php endif; ?>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		<?php endwhile; ?>
	</div>	
</div>	
<?php endif; ?>