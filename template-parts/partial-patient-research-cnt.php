<?php
/**
 * ===============================
 * PARTIAL PATIENT RESEARH CNT .PHP - display section on text
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$research_cnt = get_field('research_cnt');

?>


<div class="patient__research-cnt">
	<div class="container">
		<?php _e( $research_cnt , 'cbk' ); ?>
	</div>	
</div>



