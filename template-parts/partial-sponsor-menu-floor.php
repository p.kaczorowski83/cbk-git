<?php
/**
 * ===============================
 * PARTIAL SPONSOR MENU FLOOR .PHP - display menu floor
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="sponsor__floor">
<?php wp_nav_menu( array( 'theme_location' => 'floor-menu') ); ?>	
</div>
