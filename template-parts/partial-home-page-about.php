<?php
/**
 * ===============================
 * HOME PAGE ABOUT.PHP - home page about section
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
$home_about = get_post_meta(get_the_ID(), 'home_about', true );
$home_about_cnt = get_post_meta(get_the_ID(), 'home_about_cnt', true );
$home_about_link = get_post_meta(get_the_ID(), 'home_about_link', true );
$home_about_bg = get_field('home_about_bg');

$allowed_types = array(
	'br'     => array(),
	'strong' => array(),
	'p'      => array(),
);
?>

<section class="home__about">

    <img src="<?php echo $home_about_bg;?>">

    <!-- BOX -->
    <div class="home__about-box">
        <h1 class="typo1"><?php esc_html_e( $home_about, 'cbk' ); ?></h1>
        <p><?php echo wp_kses( __( $home_about_cnt, 'cbk' ), $allowed_types ); ?></p>
        <?php if ($home_about_link) :?>
        <div class="text-center">
        	<a href="<?php echo esc_url( $home_about_link['url'] ); ?>" class="btn__base"><?php echo esc_html_e( $home_about_link['title'],'cbk' ); ?></a>
        </div>
        <?php endif;?>
    </div>

</section>

