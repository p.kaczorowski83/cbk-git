<?php
/**
 * ===============================
 * PARTIAL ABOUT VIDEO .PHP - display video section on about page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

$video_title = get_post_meta(get_the_ID(), 'video_title', true );
$video_mp4 = get_field('video_mp4');
$video_img = get_field('video_img');
?>

<?php if ( $video_mp4 ) :  ?>
<div class="about__video">
	<div class="container">	

		<h2><?php esc_html_e( $video_title , 'cbk' ); ?></h2>

		<div class="about__video-item">
			<a data-fancybox href="<?php echo $video_mp4['url'];?>" class="fancybox">
				<figure>
					<img loading="lazy" class="lazyload img-fluid" data-src="<?php echo esc_url( $video_img['sizes']['image1240'] ); ?>" alt="<?php echo esc_attr( $video_img['alt'] ); ?>" />
				</figure>
			</a>
		</div>

		
	</div>	
</div>
<?php endif;?>



