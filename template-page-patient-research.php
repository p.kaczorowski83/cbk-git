<?php
/**
 * ===============================
 * TEMPLATE-PAGE-PATIENT-RESEARCH.PHP - template for patient page medical examination
 * ===============================
 *
 * Template name: Badania kliniczne
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'patient-research-box' );
	get_template_part( 'template-parts/partial', 'patient-research-cnt' );
	get_template_part( 'template-parts/partial', 'info-box' );
	get_template_part( 'template-parts/partial', 'menu-patient' );
	?>

</main>

<?php
get_footer();
