<?php
/**
 * ===============================
 * TEMPLATE-PAGE-SPONSOR-GROUND FLOOR.PHP - template for 2 floor in sponsor page
 * ===============================
 *
 * Template name: Piętro 2
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'sponsor-floor-2' );
	get_template_part( 'template-parts/partial', 'gallery-small' );
	get_template_part( 'template-parts/partial', 'menu-sponsor' );
	?>

</main>

<?php
get_footer();
