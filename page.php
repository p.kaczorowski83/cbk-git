<?php
/**
 * ===============================
 * PAGE.PHP - The template for displaying default theme page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
$content = get_field('content');
?>

<main class="main <?php if (is_page(123)):?>main-page<?php else :?>main-default<?php endif;?>">
    <div class="container">
        <?php echo $content;?>
    </div><!-- edn /.container -->
</main>


<?php
get_footer();

