<?php
/**
 * ===============================
 * TEMPLATE-PAGE-ABOUT-TEAM.PHP - template for team page
 * ===============================
 *
 * Template name: Zespół
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'about-team' );
	get_template_part( 'template-parts/partial', 'info-box' );
	?>

</main>

<?php
get_footer();
