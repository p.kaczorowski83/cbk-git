<?php
/**
 * ===============================
 * TEMPLATE-PAGE-PATIENT-FAQ.PHP - template for patient page faq
 * ===============================
 *
 * Template name: FAQ
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'patient-faq' );
	get_template_part( 'template-parts/partial', 'info-box' );
	get_template_part( 'template-parts/partial', 'menu-patient' );
	?>

</main>

<?php
get_footer();
