<?php
/**
 * ===============================
 * INDEX.PHP - The template for displaying content in the home page
 * Template Name: Strona główna
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php 
	get_template_part('template-parts/partial', 'home-page-slider');
	get_template_part('template-parts/partial', 'home-page-about');
	get_template_part('template-parts/partial', 'home-page-select-group');
	get_template_part('template-parts/partial', 'home-page-quotation');
	?>

</main>

<?php
get_footer();
