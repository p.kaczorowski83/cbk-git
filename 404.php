<?php
/**
 * Template Name: Not found
 * Description: Page template 404 Not found
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();

$title = esc_html( get_field( '404_title','options' ) );
$cnt = get_field( '404_cnt','options' );
$img = get_field( '404_img','options' );
$image = get_post_meta( get_the_ID(), 'options_404_img', true );
$allowed_types = array(
	'br'     => array(),
	'strong' => array(),
	'p'      => array(),
);
?>

<main class="main page404">
	
	<div class="container">
		<?php if ($img) :?>
			<img src="<?php echo esc_url( $img['url'] ); ?>" alt="">
		<?php endif?>
		<h2 class="typo2a text-center"><?php echo $title?></h2>
		<p><?php echo wp_kses( __( $cnt, 'cbk' ), $allowed_types ); ?></p>
		<div class="text-center">	
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn__orange"><?php if(ICL_LANGUAGE_CODE=='en'): ?>Home page<?php else :?>Strona główna<?php endif;?></a>
		</div>
	</div><!-- end .container -->

</main>



<?php
get_footer();
