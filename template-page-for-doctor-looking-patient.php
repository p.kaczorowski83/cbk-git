<?php
/**
 * ===============================
 * TEMPLATE-PAGE FOR DOCTOR LOOKING PATIENT.PHP - template for doctor page - looking patient
 * ===============================
 *
 * Template name: Poszukujemy pacjentów
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'doctor-looking-patient' );
	get_template_part( 'template-parts/partial', 'menu-doctor' );
	?>

</main>

<?php
get_footer();
