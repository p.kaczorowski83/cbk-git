<?php
/**
 * ===============================
 * TEMPLATE-PAGE-SPONSOR-GROUND FLOOR.PHP - template for ground floor in sponsor page
 * ===============================
 *
 * Template name: Parter
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'sponsor-ground-floor' );
	get_template_part( 'template-parts/partial', 'gallery-small' );
	get_template_part( 'template-parts/partial', 'menu-sponsor' );
	?>

</main>

<?php
get_footer();
