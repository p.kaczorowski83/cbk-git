<?php
/**
 * ===============================
 * TEMPLATE-PAGE-ABOUT.PHP - template for about page
 * ===============================
 *
 * Template name: O nas
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'about-why-us' );
	get_template_part( 'template-parts/partial', 'about-boxes' );
	get_template_part( 'template-parts/partial', 'about-gallery-rooms' );
	get_template_part( 'template-parts/partial', 'about-video' );
	get_template_part( 'template-parts/partial', 'info-box' );
	?>

</main>

<?php
get_footer();
