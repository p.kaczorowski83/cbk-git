<?php
/**
 * ===============================
 * TEMPLATE-PAGE-PATIENT-HOW.PHP - template for page with boxes 50/50
 * ===============================
 *
 * Template name: Jak sie zglosic
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'box-50-50-left' );
	get_template_part( 'template-parts/partial', 'patient-form' );
	get_template_part( 'template-parts/partial', 'info-box' );
	get_template_part( 'template-parts/partial', 'menu-patient' );
	?>

</main>

<?php
get_footer();
