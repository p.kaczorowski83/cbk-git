<?php
get_header();
global $wp_query;
?>

<?php 
$img_naglowek = get_field('img_naglowek','options');?>
<?php $naglowek_subtext = get_field('naglowek_subtext');?>

<!-- ================== [NAGŁOWEK] ================== -->
<div class="subheader" style="background: url('<?php echo $img_naglowek['url'];?>') top left no-repeat">    
    <div class="container"> 
        <h1><?php the_title();?></h1>
        <?php if($naglowek_subtext):?><span><?php echo $naglowek_subtext;?></span><?php endif?>     
    </div>
</div>

<div class="post">
    <div class="container"> 

        <div class="row">
            <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="filtr__wiget">

                    <?php $sort = $_GET['sort'];
                    $post_date_start = $_GET['post_date_start'];
                    $post_date_end = $_GET['post_date_start'];
                    ?>
                    
                    <form action="" method="get">
                    Użyj filtrów, aby szybko odnaleźć interesujący Cię artykuł:

                    <div class="row ma20">
                        <strong>Artykuły od</strong>
                        <input type="date" name="post_date_start" value=""/>
                    </div>

                    <div class="row ma20">
                        <strong>Artykuły do</strong>
                        <input type="date" name="post_date_end" value=""/>
                    </div>

                    <div class="row ma20">
                        <strong>Pokaż według:</strong>
                        <?php $sort = isset( $_GET['sort'] ) ? $_GET['sort'] : 'post_date'; ?>
                        <label><input <?php echo checked( $sort, 'post_date' ); ?> type="radio" name="sort" value="post_date"/> Ostatnio opublikowane</label>
                        <label><input <?php echo checked( $sort, 'views' ); ?> type="radio" name="sort" value="views"/> Najpopularniejsze</label>
                    </div><!-- end .row -->
                    <input type="submit" class="btn__base" value="Filtruj"/>
                    </form>
                </div>
            </div>

            <div class="col col-lg-9 col-md-9 col-sm-12 col-12">

                <?php if ($sort == 'views') :?>
                         <?php $the_query = new WP_Query( array('posts_per_page'=>10, 'date_query'     => array( 'after' => $_GET['post_date_start'], 'before' => $_GET['post_date_end'] ), 'meta_key' => 'post_views_count', 'orderby'=> 'meta_value', 'order' => 'desc',  'post_status' => 'publish', 'paged' => get_query_var('paged') ? get_query_var('paged') : 1) ); ?>
                    <?php elseif ($sort == 'post_date') :?>
                         <?php $the_query = new WP_Query( array('posts_per_page'=>10, 'date_query'     => array( 'after' => $_GET['post_date_start'], 'before' => $_GET['post_date_end'] ), 'orderby' => 'date', 'order' => 'DESC', 'post_status' => 'publish', 'paged' => get_query_var('paged') ? get_query_var('paged') : 1) ); ?>
                    <?php else:?>
                    <?php $the_query = new WP_Query( array(
                    'posts_per_page'=>10,  
                    'orderby' => 'date', 
                    'order' => 'DESC', 
                    'post_status' => 'publish',
                    'paged' => get_query_var('paged') ? get_query_var('paged') : 1
                        ) 
                    ); 
                    ?>
                    <?php endif;?>
                    
                <p><strong>Dostępne artykuły:</strong> <?php $totalpost = $the_query->found_posts; echo $totalpost; ?> </p>
                <ul>

                    


                   

			  <?php if ( $the_query->have_posts() ) : ?>
			  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php 
                        $news_foto = get_field('news_foto');
                        $news_tresc = get_field('news_tresc');
                        ?>
                        <li>
                            <a href="<?php the_permalink();?>">
                            <h3><?php the_title();?></h3>
                            <span>
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#EE5195;}
</style>
<g>
    <g>
        <path class="st0" d="M452,40h-24V0h-40v40H124V0H84v40H60C26.9,40,0,66.9,0,100v352c0,33.1,26.9,60,60,60h392
            c33.1,0,60-26.9,60-60V100C512,66.9,485.1,40,452,40z M472,452c0,11-9,20-20,20H60c-11,0-20-9-20-20V188h432V452z M472,148H40v-48
            c0-11,9-20,20-20h24v40h40V80h264v40h40V80h24c11,0,20,9,20,20V148z"/>
    </g>
</g>
<g>
    <g>
        <rect x="76" y="230" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="156" y="230" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="236" y="230" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="316" y="230" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="396" y="230" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="76" y="310" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="156" y="310" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="236" y="310" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="316" y="310" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="76" y="390" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="156" y="390" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="236" y="390" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="316" y="390" class="st0" width="40" height="40"/>
    </g>
</g>
<g>
    <g>
        <rect x="396" y="310" class="st0" width="40" height="40"/>
    </g>
</g>
</svg>
 <?php echo the_time('F j, Y');?></span>
                            <?php echo mb_substr(strip_tags($news_tresc), 0, 410)."...";?>
                            </a>
                        </li>
                    <?php endwhile; ?>
                <?php endif;?>
                </ul>
                
                <div class="paginate__links">
                    <?php   $big = 999999999; // need an unlikely integer
                        echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );
                    wp_reset_postdata();?>  
                </div>
            </div>
        </div>


    </div><!-- end .container -->

</div><!-- end .post -->

<?php get_footer(); 