<?php
/**
 * ===============================
 * TEMPLATE-PAGE-SETTLEMENTS.PHP - template for settlements page
 * ===============================
 *
 * Template name: Dane rozliczeniowe
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<div class="container">
		
		<section class="contact">
		<?php
		get_template_part( 'template-parts/partial', 'contact-left-coll' );
		get_template_part( 'template-parts/partial', 'contact-right-coll' );
		?>
		</section>

	</div>

	<?php
		get_template_part( 'template-parts/partial', 'menu-sponsor' );
	?>

</main>

<?php
get_footer();
