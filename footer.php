<?php
/**
 * ===============================
 * FOOTER.PHP - footer faile
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<footer id="footer">
    <?php 
    get_template_part( 'template-parts/partial', 'footer-emergency-contact' ); 
    ?>
    <div class="container">
        <div class="footer__items">
            <?php get_template_part( 'template-parts/partial', 'footer-col-1' ); 
            get_template_part( 'template-parts/partial', 'footer-col-2' );
            ?>
        </div>
    </div>

    <div class="footer__association">
        <div class="container">
            <?php get_template_part( 'template-parts/partial', 'footer-association' ); ?>
        </div>
    </div>

        <div class="footer__copy">
            <div class="container">
            <ul>
                <li> Copyrights © <?php echo date('Y');?> <strong>Centrum Badań Klinicznych</strong></li>
                <li> <?php if(ICL_LANGUAGE_CODE=='en'): ?>Design and code<?php else :?>Projekt i realizacja<?php endif;?> <a href="https://qualitypixels.pl/projektowanie-stron-internetowych/" target="_blank" title="Quality Pixels">Quality Pixels</a></li>
            </ul>
            </div>
        </div>
</footer><!-- /#footer -->
</div>




<?php wp_footer(); ?>
<script>
WebFontConfig = {
    google: { families: ['Asap:wght@400;700:latin-ext&display=swap'] }
};

(function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
</script>
</body>

</html>