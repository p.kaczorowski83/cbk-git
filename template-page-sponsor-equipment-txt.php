<?php
/**
 * ===============================
 * TEMPLATE-PAGE FOR SPONSOR EQUIOMENT.PHP - template for sponsor page - equipment
 * ===============================
 *
 * Template name: Wyposażenie techniczne
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main">

	<?php
	get_template_part( 'template-parts/partial', 'doctor-looking-patient' );
	get_template_part( 'template-parts/partial', 'menu-sponsor' );
	?>

</main>

<?php
get_footer();
