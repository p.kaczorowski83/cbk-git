<?php
/**
 * ===============================
 * SEARCH.PHP - The Template for displaying Search Results pages.
 * ===============================
 *
 * @package NID
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
global $wp_query;
$total_results = $wp_query->found_posts;
$ss = $_GET["s"];
?>

<main class="main main-page">
  <div class="container">
    
    <?php if (have_posts()) :?>
    <div class="text-center"> 
      <h2>Wyniki wyszukiwania <?php if($ss):?>"<?php echo $ss;?>"<?php endif;?></h2>
      <h3>Ilość znalezionych wpisów: <?php echo $total_results;?></h3>
    </div> 

      <ul class="list__post">
        
          <?php  while (have_posts()) : the_post(); 
            $content = get_the_content();
            $content = strip_tags($content);
            $foto_news = get_post_meta(get_the_ID(), 'foto_news', true );
            $image = wp_get_attachment_image_src($foto_news, 'img-post');
            $categories = get_the_terms($post->ID, 'category');
            ?>
              <li>
            <a href="<?php the_permalink(); ?>" class="related-product">
                <div class="list__post-foto">
                    <div>
                        <?php if ($image):?>
                            <img src="<?php echo $image[0];?>">
                        <?php else :?>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/svg/photo.svg" class="img-fluid" alt="">
                        <?php endif;?>
                    </div>
                </div>
                <div class="list__post-cnt">
                    <h3><?php the_title();?></h3>                   
                    <?php echo mb_substr($content, 0, 385); ?>...
                    <div class="text-right">
                        <div class="button__red">przeczytaj więcej</div>
                    </div>
                </div>
            </a>
        </li>
          <?php endwhile;?>
      </ul>

      <div class="paginate__links">
        <?php echo paginate_links( array('prev_text' => '<svg version="1.1" id="Warstwa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve"><style type="text/css">.st0{fill:#FFFFFF;}.st1{fill:none;stroke:#000000;stroke-width:4;}</style><rect x="0" y="0" class="st0" width="40" height="40"/><path class="st1" d="M23,14l-6,6l6,6"/></svg> poprzednia','next_text' => 'następna <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="40" height="40" fill="white"/><path d="M17 26L23 20L17 14" stroke="black" stroke-width="4"/></svg>')); ?>
        </div>

    <?php else :?>

        <div class="text-center"> 
            <span class="search404"> 
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-search-problem.svg" alt="" class="img-fluid">
            </span>

            <h2>Nie znaleźliśmy wyników wyszukiwania.</h2>
            <h3>Wprowadź proszę inną frazę.</h3>
        </div>  

    <?php endif;?>
    
  </div><!-- end /. container -->
</main>


   <?php get_footer();