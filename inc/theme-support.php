<?php
/**
 * ===============================
 * THEME SUPPORT OPTIONS
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */

if ( ! function_exists( 'cbk_setup_theme' ) ) :
	function cbk_setup_theme() {

		// Make theme available for translation: Translations can be filed in the /languages/ directory
		load_theme_textdomain( 'cbk', get_template_directory() . '/languages' );

		// Theme Support
		add_theme_support( 'title-tag' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		) );

		add_filter(
		    'wp_lazy_loading_enabled',
		    function( $result, $tag_name ) {
		        if ( 'img' === $tag_name ) {
		            return false;
		        }
		        return $result;
		    },
		    10,
		    2
		);

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );
		// Add support for editor styles.
		add_theme_support( 'editor-styles' );
		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Default Attachment Display Settings
		update_option( 'image_default_align', 'none' );
		update_option( 'image_default_link_type', 'none' );
		update_option( 'image_default_size', 'large' );

		// Custom CSS-Styles of Wordpress Gallery
		add_filter( 'use_default_gallery_style', '__return_false' );

	}
	add_action( 'after_setup_theme', 'cbk_setup_theme' );
endif;

/**
 * Set the max image width.
 */
function cbk_max_srcset_image_width() {
	return 2560;
}
add_filter( 'max_srcset_image_width', 'cbk_max_srcset_image_width', 10, 2 );

/**
 * Add custom image sizes
 */
// function cbk_add_image_sizes() {
// 	add_image_size('img-post',190, 190, true);
// 	add_image_size('img-slider',400, 400, true);
// 	add_image_size('img-poster', 380);
// }
// add_action( 'init', 'cbk_add_image_sizes' );

function cbk_add_image_sizes() {
	add_image_size( 'image600', 600 );
	add_image_size( 'image700', 700, 500, true );
	add_image_size( 'image610', 610, 450, true );
	add_image_size( 'image200', 200, 200, true );
	add_image_size( 'image295', 295, 195, true );
	add_image_size( 'image388', 388, 388, true );
	add_image_size( 'image1200', 1200 );
	add_image_size( 'image1240', 1240 );
	add_image_size( 'image1920', 1920 );
}
add_action( 'init', 'cbk_add_image_sizes' );

/**
 * Remove default WP image sizes
 *
 * @param array $sizes Array of media image sizes.
 *
 * @return array
 */
function cbk_remove_default_images( $sizes ) {
	unset( $sizes['medium'] ); // 300px
	unset( $sizes['large'] ); // 1024px
	unset( $sizes['medium_large'] ); // 768px
	return $sizes;
}
add_filter( 'intermediate_image_sizes_advanced', 'cbk_remove_default_images' );

/**
 * Nav menus
 *
 * @since v1.0
 */
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'main-menu'   => 'Main Navigation Menu',
			'footer-menu' => 'Footer Menu',
			'floor-menu' => 'Floor Menu',
		)
	);
}


// Custom Nav Walker: wp_bootstrap4_navwalker()
$custom_walker = get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
if ( is_readable( $custom_walker ) ) {
	require_once $custom_walker;
}

$custom_walker_footer = get_template_directory() . '/inc/wp_bootstrap_navwalker_footer.php';
if ( is_readable( $custom_walker_footer ) ) {
	require_once $custom_walker_footer;
}


// CURRENT MENU
function my_special_nav_class( $classes, $item ) {
 
    if ( $item->ID == 325 ) {
    	if ( get_query_var( 'baza_wiedzy' )) {
            $classes[] = 'current-menu-item';
        }
    }
    elseif ( $item->ID == 329 ) {
    	if ( get_query_var( 'akty-prawne' )) {
            $classes[] = 'current-menu-item';
        }
    }
    elseif ( $item->ID == 328 ) {
    	if ( get_query_var( 'publikacje' )) {
            $classes[] = 'current-menu-item';
        }
    }
    elseif ( $item->ID == 2008 ) {
    	if ( get_query_var( 'rewitalizacja' ) || is_tax('rewitalizacja-cat') ) {
            $classes[] = 'current-menu-item';
        }
    }
 
    return $classes;
 
}

add_filter('show_admin_bar', '__return_false');


