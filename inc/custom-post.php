<?php
// Register Custom Post Type
function custom_post_badania() {

  $labels = array(
    'name'                  => _x( 'Badania', 'Post Type General Name', 'cbk' ),
    'singular_name'         => _x( 'Badania', 'Post Type Singular Name', 'cbk' ),
    'menu_name'             => __( 'Badania', 'cbk' ),
    'name_admin_bar'        => __( 'Post Type', 'cbk' ),
  );
  $args = array(
    'label'                 => __( 'Badania', 'cbk' ),
    'description'           => __( 'Post Type Description', 'cbk' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor' ),
    'taxonomies'            => array( 'kat-badania' ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'           => 'dashicons-thumbs-up',
  );
  register_post_type( 'aktualne-badania', $args );

}
add_action( 'init', 'custom_post_badania', 0 );

// Register Custom Taxonomy
function custom_badania() {

  $labels = array(
    'name'                       => _x( 'Aktualnie prowadzone badania -', 'Taxonomy General Name', 'cbk' ),
    'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'cbk' ),
    'menu_name'                  => __( 'Kategorie', 'cbk' ),
    'all_items'                  => __( 'Wszystkie badania', 'cbk' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'kat-badania', array( 'aktualne-badania' ), $args );

}
add_action( 'init', 'custom_badania', 0 );
